var generalParameters;
var murcaParameters;
var durcaParameters;


async function start() {
  // Ottieni i parametri da Python utilizzando Eel
  const parameters = await eel.get_parameters()();

  // Converti le stringhe JSON in oggetti JavaScript
  generalParameters = JSON.parse(parameters.inputlist);
  murcaParameters = JSON.parse(parameters.murcalist);
  durcaParameters = JSON.parse(parameters.durcalist);


  // Crea dinamicamente l'interfaccia utente in base ai parametri
  createUI(generalParameters, 'inputlist');
  createUI(murcaParameters, 'murcalist');
  createUI(durcaParameters, 'durcalist');

  // Aggiungi eventi per modific"are i parametri
  addInputEvents(generalParameters,'inputlist');
  addInputEvents(murcaParameters,'murcalist');
  addInputEvents(durcaParameters,'durcalist');

}

function createUI(params, prefix) {
  const container = document.getElementById(prefix);

  for (const key in params) {
    const param = params[key];

    const div = document.createElement('div');

    const label = document.createElement('label');
    label.title = param.helpstr;
    label.textContent = param.name + ':';
    label.htmlFor = prefix + '_' + key;
    const linebreak = document.createElement("br");
    let input;

    switch (param.type){
    case "choice":
      input = document.createElement("select");
      input.classList.add("params","contr");

      //Create and append the options
      for (const [kk, value] of Object.entries(param.contr)) {
	var option = document.createElement("option");
	option.value = kk;
	option.text = kk;
	input.appendChild(option);
      }
      input.value = param.default;
      break;

    case "int":
      input = document.createElement('input');
      input.classList.add("params","int");
      input.type = 'number';
      input.step = 1;
      input.min = 0;
      input.value = param.default;
      break;

    case "float":
      input = document.createElement('input');
      input.type = 'number';
      input.step = 'any';
      input.classList.add("params","float");
      input.value = param.default;
      break;

    case "int3":
      input  = [];
      for(let i=0;i<3;i++ ){
	//input.push;
	let a = document.createElement('input');
	a.type = "numeber";
	a.classList.add("params","int3");
	a.value = param.default[i];
	a.step = 1;
	a.min = 0;
	a.id = prefix + '_' + key +'_'+i;
	input.push(a);
      }
      break;

    case "bool":
      input = document.createElement('input');

      input.classList.add("params");
      input.type = 'checkbox';
      input.checked = param.default;

      break;

    default:
      input = document.createElement('input');
      input.classList.add("params");
      input.type = 'text';
      input.value = param.default;

    }
    div.appendChild(label);

    if(param.type!=="int3"){
      input.id = prefix + '_' + key;
      div.appendChild(input);
    }
    else{
      div.appendChild(input[0]);
      div.appendChild(input[1]);
      div.appendChild(input[2]);
    }

    // level 0 or 1 are hidden
    div.classList.add("level_"+param.level);

    // add line to form
    container.append(div);

  }
}

function addInputEvents(params,base) {
  for (const key in params) {
    const name = base+'_'+key ;
    const input = document.getElementById(name);
    // console.log(name,input)
    if (input) {
      //******************mettere controllo MARCO **************
      input.addEventListener("change",event => {
	input.classList.toggle('--error');
      });

      // input.addEventListener('input', function(event) {
      //   params[key].value = event.target.default;
      //   console.log("param",params[key])
      // });
    }
  }
}



async function saveParams() {
  if (!validateFields()) {
    // alert('One ore more fields are not valid. Corntrol it.');
    return;
  }

  // GET jsoon file name
  const input = document.getElementById("json_dump_file");

  const filename = (input.value) ? input.value : input.placeholder;


  // Ottieni i valori aggiornati dagli input dell'utente
  const updatedParams = {
    'inputlist' : getUpdatedValues('inputlist',generalParameters),
    'murcalist' : getUpdatedValues('murcalist',murcaParameters),
    'durcalist' : getUpdatedValues('durcalist',durcaParameters),
  };

  // Salva i parametri aggiornati utilizzando Eel
  await eel.save_parameters(updatedParams,filename,true)();

}


async function saveAndRun() {
  saveParams();

  // GET jsoon file name
  let input = document.getElementById("json_dump_file");
  const filename = (input.value) ? input.value : input.placeholder;


  // GET nproc
  input = document.getElementById("MPI_nproc");
  const mpi_nproc = (input.value) ? input.value : input.placeholder;

  // GET nthreads
  input = document.getElementById("OMP_threads");
  const omp_threads = (input.value) ? input.value : input.placeholder;


  // Chiama run_application di Eel
  await eel.run_application(filename,mpi_nproc,omp_threads,false)();


}


function getUpdatedValues(prefix,params) {
  for (const key in params) {
    //if(params[key].level !== 0) continue;

    if(params[key].type==="int3"){
      params[key]["value"] = [];
      for(let i=0;i<3;i++){
	const input = document.getElementById(prefix + '_' + key+"_"+i.toString());
	params[key]["value"].push(parseInt(input.value));
	//console.log(key,params[key]["value"])
      }
    }
    else{
      const input = document.getElementById(prefix + '_' + key);

      if (input) {
	switch (params[key].type.trim()){
	case "int" :
	  params[key]["value"] = parseInt(input.value);
	  break;
	case "float" :
	  params[key]["value"] = parseFloat(input.value);
	  break;
	case "bool" :
	  params[key]["value"] = input.checked;
	  break;
	case "choice" :
	  params[key]["value"] = input.value;
	  break;
	default:
	  alert("Error for :", key);
	  break;
	}
      }
    }

    //console.log(key,params[key]["value"]);
  }


  return params;
}

function validateFields() {
  // Aggiungi qui la logica di validazione dei campi
  const inputChoiceProgram = document.getElementById('inputlist_choice_program');
  const inputChoiceValue = inputChoiceProgram.value;

  // Lista di valori validi per 'choice_program'
  const validChoiceProgramValues = generalParameters.choice_program.contr;
  console.log(validChoiceProgramValues)


  // Controlla se il valore is valido
  if (!Object.keys(validChoiceProgramValues).includes(inputChoiceValue)) {
    inputChoiceProgram.classList.toggle('--error');
    return false;
  }

  // Aggiungi ulteriori controlli per gli altri campi
  // ...

  // Se tutti i controlli passano, restituisci true
  return true;
}




function updateUI(params) {
  // loop on set of main keys : inputlist, murcalist, durcalist
  for (const prefix in params) {
    const param2 = params[prefix];

    for (const [key,val2] of Object.entries(param2)) {
      let changed = false;
      let actual;
      // console.log(key,val2)
      if(val2.type =='int3'){
	for(let i=0;i<3;i++){
	  const read_val = val2.value[i].toString();
	  actual = document.getElementById(prefix + '_' + key+"_"+i.toString());

	  if(actual.value !== read_val){
	    actual.value = read_val;
	    actual.parentNode.classList.remove("level_1");
	    actual.parentNode.classList.add("level_0");
	    actual.classList.add("--blue");
	  }
	}

	// console.log(key);
	// console.log(actual,prefix+"_"+key,val2.value)

      }
      else{
	actual = document.getElementById(prefix + '_' + key);
	// console.log(actual.value)
	// console.log(val2)
	const read_val = val2.value.toString();

	switch (val2.type){
	case 'float':
	case 'int':
	  if(actual.value !== read_val){
	    actual.value = val2.value;
	    changed = true;
	  }
	  break;

	case "bool":
	  if(actual.checked !== val2.value){
	    actual.checked = val2.value;
	    changed = true;
	  }
	  break;

	case "choice":
	  if(actual.value !== read_val){
	    if(actual.querySelector('[value="' + read_val + '"]'))
	    {
	      changed=true;
	      actual.value = read_val;
	    }
	    else{
	      alert('Error: the option "'+read_val+'" for "'+key+
		    '" in "'+prefix+'" is not valid.' + '\nPlease control'
		    + ' uploaded file !');
	      return;
	    }
	  }
	  break;

	default:
	  alert('The type "'+val2.type+'" of the variable "'+key+'" in "'
		+ prefix +'" of the uploaded file is not recognized.'+
		'\nPlease control the file');
	  return;

	}
      }


      if(changed){
	actual.parentNode.classList.remove("level_1");
	actual.parentNode.classList.add("level_0");
	actual.classList.add("--blue");
      }

    }

  }

}

function uploadFile() {
  // Get the input element and the selected file
  const input = document.getElementById('fileInput');
  const file = input.files[0];
  let jsonData;
  if (file) {
    // Read the file as text
    var reader = new FileReader();
    reader.onload = function(e) {
      // Parse the JSON content
      var jsonData = JSON.parse(e.target.result);

      // You can now use jsonData to fill the form or perform other actions
      // For demonstration, let's log the JSON data to the console
      updateUI(jsonData);
    };

    // Read the file as text
    reader.readAsText(file);
  } else {
    alert("Please select a file");
  }


  //await eel.update_form(jsonData)();
}

function quitApp() {
  // Call the Python function to quit the app
  if (confirm("Close Window?")) {
    close();
    eel.quit_app();
  }
}

start();  // Avvia la funzione start quando la pagina Ã¨ pronta
