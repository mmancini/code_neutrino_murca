"""
This module contains the dict for the creation of the input variables:
general_parameters,
murca_parameters,
durca_parameters.
They have to be containings the keys which permits to initialise the Variable class

"""
from pydantic import BaseModel #, ConfigDict, Field
from typing import Generic, TypeVar
import json

TypeX = TypeVar('TypeX')


class ParameterClass(BaseModel, Generic[TypeX]):
    name    : str
    unit    : str = ""
    helpstr : str = ""
    #typeq    : str | None = None
    level   : int = 0
    contr   : dict[str,int] | None = None
    default : TypeX | None = None
    value  : TypeX | None = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.value is None :
            self.value = self.default
    def __repr__(self):
        return  ("").join((f"{ii[0]} = {ii[1]}\n" for ii in self))


inputlist_0 = {
    "choice_program" :  {
        "name" : "choice_program",
        "default"  : "3",
        "contr" : {"1":1,"2":2,"3":3,"4":4},
        "helpstr" : "\n(1) Computations of rates for Durca reactions"+
        "\n(2) Several tests related to the Durca computations"+
        "\n(3) Computations of rates for Murca reactions"+
        "\n(4) Tests of Fermi speed for non-relativistic EoS models",
        "type":"choice",
        "level":0
    },
    "grid_point" : {
        "name" : "ijkn_in",
        "default" : [1,1,1],
        "helpstr" : """
            chosen point if cheb
            """,
        "type":"int3",
        "level":0
    },
    "temperature" : {
        "name" : "temperature",
        "default" : 30.,
        "unit" : "MeV",
        "helpstr" : "\n Value of the Temperature for calculation.",
        "type":"float",
        "level":0
    },
    "density" : {
        "name" : "density",
        "default" : 4.0e-1,
        "unit" : "fm-3",
        "helpstr" : """
        Value of the Baryon number density for calculation.
        """,
        "type":"float"
,
        "level":0
    },
    "electron_fraction" : {
        "name" : "electron_fraction",
        "default" : 0.17,
        "unit" : "",
        "helpstr" : """
            Value of the electron fraction for calculation.
        """,
        "type":"float",
        "level":0
    },
    "mf" : {
        "name" : "mf",
        "default" : "1",
        "unit" : "",
        "helpstr" : """
            which type of (effective) masses and chemical potentials
            mf = 1: take mean field masses/chemical potentials
            mf = 2: take free masses/chemical potentials
            """,
        "contr" : {"1":1,"2":2},
        "type":"choice",
        "level":0
    },
    "eos_type" : {
        "name" : "eos_type",
        "default" : "RMF",
        "unit" : "",
        "contr" : {"Skyrme":1,"APR":2,"Virial":3,"RMF":4},
            "helpstr" : """
            (1) Skyrme  (2) APR (3) Virial (4) RMF
            """,
        "type":"choice",
        "level":0
    },
    "omega" : {
        "name" : "omega",
        "unit" : "MeV",
        "default" : 5.0,
        "helpstr" : """
            (Anti-)neutrino energy for calculations
            """,
        "type":"float",
        "level":0
    },
    "readspp" : {
        "name" : "readspp",
        "default" : "2",
        "unit" : "",
        "contr": {"1":1,"2":2},
        "helpstr" : """
            (1) read table with interaction potentials
            and generate new eos.micro.new, only avaiable for Matthias Hempel's relativistic tables
            (2) read from Compose eos.micro (if present)
            """,
        "type":"choice",
        "level":1
    },
    "enumin" : {
        "name" : "enumin",
        "unit" : "MeV",
        "default" : 0.1,
        "helpstr" : """
            min  (anti-) neutrino energies used for plotting Murca with leptons
            """,
        "type":"float",
        "level":0
    },
    "enumax" : {
        "name" : "enumax",
        "unit" : "MeV",
        "default" : 150,
        "helpstr" : """
        max  (anti-) neutrino energies used for plotting Murca with leptons
        """,
        "type":"float",
        "level":0
    },

}


murcalist_0 = {
    "choice_hadron" : {
        "name" : "choice_hadron",
        "default" : "4",
        "contr" : {"1":1,"2":2,"3":3,"4":4,"5":5,"6":6},
        "helpstr" : "\n(1) Full MC integration of Murca with leptons"+ 
        "\n(2) MC integration of hadronic polarisation function for Murca"+
        "\n(3) Murca with leptons, MC integration of hadronic part and Gauss-Legendre for leptons"+
        "\n(4) Computation of Murca with leptons on a grid in temperature, baryon number density and electron fraction"+
        "\n(5) Murca with muonic leptons"+
        "\n(6) (Anti-)neutrino particle production rates as function of electron fraction, comparison with Fermi surface approximation",
        "type":"choice",
        "level":0},


        "fermi_momentum" : {
        "name" : "fermi_momentum",
        "default" : "1",
        "contr" : {"0":0,"1":1},
        "helpstr" : "\n(0) Fermi surface approximation: neutron and proton Fermi momenta calculated from densities"+ 
        "\n(1) Fermi surface approximation: neutron and proton Fermi momenta calculated from chemical potentials",
        "type":"choice",
        "level":0},

    
    "nmc" : {
        "name" : "nmc",
        "default" : 96000,
        "helpstr" : """
        Number of samples for MC integration with leptons
        """,
        "type":"int",
        "level":0
    },


    "pointsenu" : {
        "name" : "pointsenu",
        "default" : 18,
        "helpstr" : """
        Number of points in (anti-)neutrino energy for Murca
        """,
        "type":"int",
        "level":0
    },

    "which_leptons" : {
        "name" : "which_leptons",
        "default" : "1",
        "contr" : {"1":1,"2":2,"3":3,"4":4},
        "helpstr" : "\n(1) Electron capture and inverse"+
	"\n(2) Positron capture and inverse"+
        "\n(3) Neutron decay and inverse"+
	"\n(4) Proton decay and inverse",
        "type":"choice",
        "level":0
    },

    "nenb" : {
        "name" : "nenb",
        "default" : 1,
        "helpstr" : """
        Calculate reactions with incoming charged leptons (1) or outgoing (2)
        """,
        "type":"int",
        "level":1
    },
    "gamma_width" : {
        "name" : "gamma_width",
        "default" : 35.0,
        "helpstr" : """
        Nucleon self-energy (MeV) for Murca calculations
        """,
        "type":"float",
        "level":0
    },
    "width" : {
        "name" : "width",
        "default" : 0.5,
        "helpstr" : """
        Width for cosh sampling of MC integration
        """,
        "type":"float",
        "level":1
    },

    "limitq0" : {
        "name" : "limitq0",
        "default" : 0.1,
        "helpstr":""" 
        Special treatment for q0-integration around q0=0
        """,
        "type":"float",
        "level":1
    },


    "denom_type" : {
        "name" : "denom_type",
        "default" : "full"  ,
        "contr" : {"none":0,"full":1,"ener":2},
        "helpstr" : """
        Using denominator for intermediate nucleon propagators :
         'none'  Energy exchange (q0) 
         'full' Full denominator (corresponding to FULL_DENOM precompilation flag)
         'ener' Electron energy (corresponding to EE_DENOM precompilation flag)
        """,
        "type":"choice",
        "level":0
    },

    "ngq_murca" : {
        "name" : "ngq",
        "unit" : "",
        "default" : 8,
        "helpstr" : """
        Number of points for Gauss-Legendre integration over q
        """,
        "type":"int",
        "level":0
    },

    "ngq0_murca" : {
        "name" : "ngq0",
        "unit" : "",
        "default" : 16,
        "helpstr" : """Number of points for Gauss-Legendre integration over q0
        """,
        "type":"int",
        "level":0
    },

    "tptsmin" : {
        "name" : "tptsmin",
        "unit" : "",
        "default" : 0,
        "helpstr" : """
        Index of lowest grid point in temperature for full table calculation
        """,
        "type":"int",
        "level":0
    },
    "tptsmax" : {
        "name" : "tptsmax",
        "unit" : "",
        "default" : 1,
        "helpstr" : """
        Index of highest grid point in temperature for full table calculation
        """,
        "type":"int",
        "level":0
    },
    "nptsmin" : {
        "name" : "nptsmin",
        "unit" : "",
        "default" : 0,
        "helpstr" : """
        Index of lowest grid point in n_B for full table calculation
        """,
        "type":"int",
        "level":0
    },
    "nptsmax" : {
        "name" : "nptsmax",
        "unit" : "",
        "default" : 1,
        "helpstr" : """
        Index of highest grid point in n_B for full table calculation
        """,
        "type":"int",
        "level":0
    },

    "yptsmin" : {
        "name" : "yptsmin",
        "unit" : "",
        "default" : 0,
        "helpstr" : """
        Index of lowest grid point in Y_e for full table calculation
        """,
        "type":"int",
        "level":0
    },
    "yptsmax" : {
        "name" : "yptsmax",
        "unit" : "",
        "default" : 1,
        "helpstr" : """
        Index of highest grid point in Y_e for full table calculation
        """,
        "type":"int",
        "level":0
    },

}


durcalist_0 = {
    "nlimit" : {
        "name" : "nlimit",
        "default" : 15,
        "unit" : "",
        "helpstr" : """
        """,
        "type":"int",
        "level":1
    },

    "distance_continum" : {
        "name" : "distance_continum",
        "unit" : "",
        "default" : 2.0,
        "helpstr" : """
        """,
        "type":"float",
        "level":1
    },

    "epsx" : {
        "name" : "epsx",
        "unit" : "",
        "default" : 1.e-5,
        "helpstr" : """
        """,
        "type":"float",
        "level":1
    },


    "epsrel" : {
        "name" : "epsrel",
        "unit" : "",
        "default" : 1.e-10,
        "helpstr" : """
        """,
        "type":"float",
        "level":1
    },

    "dist_standard" : {
        "name" : "dist_standard",
        "unit" : "",
        "default" : .1,
        "helpstr" : """
        """,
        "type":"float",
        "level":1
    },

    "ngq_rpa" : {
        "name" : "ngq_rpa",
        "unit" : "",
        "default" : 16,
        "helpstr" : """
        Number of points for GL integration over q, RPA
        """,
        "type":"int",
        "level":1
    },

    "ngq_rel" : {
        "name" : "ngq_rel",
        "unit" : "",
        "default" : 64,
        "helpstr" : """
        Number of points for GL integration over q, relativistic case
        """,
        "type":"int",
        "level":1
    },

    "ngq_durca" : {
        "name" : "ngq",
        "unit" : "",
        "default" : 16,
        "helpstr" : """
        Number of points for Gauss-Legendre integration over q, Lindhard case
        """,
        "type":"int",
        "level":1
    },

    "ngq0_durca" : {
        "name" : "ngq0",
        "unit" : "",
        "default" : 16,
        "helpstr" : """
        Number of points for Gauss-Legendre integration over q0, Lindhard case
        """,
        "type":"int",
        "level":1
    },

    "ngq0_rpa" : {
        "name" : "ngq0_rpa",
        "unit" : "",
        "default" : 64,
        "helpstr" : """
        Number of points for Gauss-Legendre integration over q0, RPA case
        """,
        "type":"int",
        "level":1
    },

    "ngq0_rel" : {
        "name" : "ngq0_rel",
        "unit" : "",
        "default" : 32,
        "helpstr" : """
        Number of points for Gauss-Legendre integration over q0, relativistic case
        """,
        "type":"int",
        "level":1
    },

    "pts_pol" : {
        "name" : "pts_pol",
        "unit" : "",
        "default" : 8,
        "helpstr" : """
        Number of points for each domain for interpolation of Durca results
        """,
        "type":"int",
        "level":1
    },

    "epsrpa" : {
        "name" : "epsrpa",
        "unit" : "",
        "default" : 1.e-1,
        "helpstr" : """
        """,
        "type":"float",
        "level":1
    },

    "contrast" : {
        "name" : "contrast",
        "unit" : "",
        "default" : 10.,
        "helpstr" : """
        """,
        "type":"float",
        "level":1
    },


    "max2nd" : {
        "name" : "max2nd",
        "unit" : "",
        "default" : 10.,
        "helpstr" : """
        """,
        "type":"float",
        "level":1
    },


    "lambda_threshold" : {
        "name" : "lambda_threshold",
        "unit" : "",
        "default" : 1.e-15,
        "helpstr" : """Threshold for minimum inverse neutrino mean free path
    """,
        "type":"float",
        "level":1
    },

    "choice" : {
        "name" : "choice",
        "unit" : "",
        "default" : "NR",
        "contr" : {"NR":1, "RPA":2, "R":3 , "full_RPA":4, "with_Murca":5},
        "helpstr": " Options for calculation of Durca rates: 1) Lindhard \n 2) RPA in Landau approximation \n 3) Relativistic \n 4) full_RPA \n 5) with_Murca, included via a finite quasi-particle width in Lindhard function \n",
        "type":"choice",
        "level":0
    },

    "check_domains_nu" : {
        "name" : "check_domains_nu",
        "unit" : "",
        "default" : True,
        "helpstr" : """
        """,
        "type":"bool",
        "level":1
    },

    "check_domains_nubar" : {
        "name" : "check_domains_nubar",
        "unit" : "",
        "default" : True,
        "helpstr" : """
        """,
        "type":"bool",
        "level":1
    },

    "nubar_borders" : {
        "name" : "nubar_borders",
        "unit" : "",
        "default" : False,
        "helpstr" : """
        """,
        "type":"bool",
        "level":1
    },

    "nu" : {
        "name" : "nu",
        "unit" : "",
        "default" : True,
        "helpstr" : """
        """,
        "type":"bool",
        "level":1
    },

    "nubar" : {
        "name" : "nubar",
        "unit" : "",
        "default" : True,
        "helpstr" : """
        """,
        "type":"bool",
        "level":1
    },


    "reactions" : {
        "name" : "reactions",
        "unit" : "",
        "default" : False,
        "helpstr" : """
        Store contributions to opacities from the different reactions sperataly (true) or not (false)
        """,
        "type":"bool",
        "level":0
    },


    "int_rel" : {
        "name" : "int_rel",
        "unit" : "",
        "default" : True,
        "helpstr" : """
        """,
        "type":"bool",
        "level":1
    },

    "pkte_lindhard" : {
        "name" : "pkte_lindhard",
        "unit" : "",
        "default" : 200,
        "helpstr" : """
        """,
        "type":"int",
        "level":1
    },

    "pt_min" : {
        "name" : "pt_min",
        "unit" : "",
        "default" : [1,1,1],
        "helpstr" : """
        """,
        "type":"int3",
        "level":1
    },

    "pt_max" : {
        "name" : "pt_max",
        "unit" : "",
        "default" : [1,1,1],
        "helpstr" : """
        """,
        "type":"int3",
        "level":1
    },
    "cheb": {
        "name": "cheb",
        "unit": "",
        "default": False,
        "helpstr": "\n True (grid point of EoS table) or "+
                   "\n False (given value of T, nB, Ye with interpolation of EoS table)",
        "type": "bool",
        "level":0
    },


}

class InputListClass(BaseModel):
    choice_program : ParameterClass[int]      = None
    grid_point : ParameterClass[list[int]]    = None
    temperature : ParameterClass[float]       = None
    density : ParameterClass[float]           = None
    electron_fraction : ParameterClass[float] = None
    mf : ParameterClass[int]                  = None
    eos_type : ParameterClass[str]            = None
    omega : ParameterClass[float]             = None
    readspp : ParameterClass[int]             = None
    enumin : ParameterClass[float]            = None
    enumax : ParameterClass[float]            = None

class MurcaListClass(BaseModel):
    choice_hadron  : ParameterClass[int]      = None
    nmc : ParameterClass[int]                 = None
    which_leptons : ParameterClass[int]       = None
    nenb : ParameterClass[int]                = None
    gamma_width: ParameterClass[float]        = None
    width : ParameterClass[float]             = None
    limitq0 : ParameterClass[float]           = None
    denom_type : ParameterClass[str]          = None
    ngq_murca : ParameterClass[int]           = None
    ngq0_murca : ParameterClass[int]          = None
    tptsmin : ParameterClass[int]             = None
    tptsmax : ParameterClass[int]             = None
    nptsmin : ParameterClass[int]             = None
    nptsmax : ParameterClass[int]             = None
    yptsmin : ParameterClass[int]             = None
    yptsmax : ParameterClass[int]             = None
    pointsenu : ParameterClass[int]           = None

class DurcaListClass(BaseModel):
    nlimit : ParameterClass[int]               = None
    distance_continum : ParameterClass[float]  = None
    epsx : ParameterClass[float]               = None
    epsrel : ParameterClass[float]             = None
    dist_standard : ParameterClass[float]      = None
    ngq_rpa : ParameterClass[int]              = None
    ngq_rel : ParameterClass[int]              = None
    ngq_durca : ParameterClass[int]            = None
    ngq0_durca : ParameterClass[int]           = None
    ngq0_rpa : ParameterClass[int]             = None
    ngq0_rel : ParameterClass[int]             = None
    pts_pol : ParameterClass[int]              = None
    epsrpa : ParameterClass[float]             = None
    contrast : ParameterClass[float]           = None
    max2nd : ParameterClass[float]             = None
    lambda_threshold : ParameterClass[float]   = None
    choice : ParameterClass[str]               = None
    check_domains_nu  : ParameterClass[bool]   = None
    check_domains_nubar : ParameterClass[bool] = None
    nubar_borders : ParameterClass[bool]       = None
    nu : ParameterClass[bool]                  = None
    nubar : ParameterClass[bool]               = None
    reactions : ParameterClass[bool]           = None
    int_rel : ParameterClass[bool]             = None
    pkte_lindhard : ParameterClass[int]        = None
    pt_min : ParameterClass[list[int]]         = None
    pt_max : ParameterClass[list[int]]         = None
    cheb : ParameterClass[bool]                = None


class NeutrinoParameterClass(BaseModel):
    inputlist : InputListClass
    murcalist : MurcaListClass
    durcalist : DurcaListClass

parameters = NeutrinoParameterClass(
    inputlist = InputListClass(**inputlist_0),
    murcalist = MurcaListClass(**murcalist_0),
    durcalist = DurcaListClass(**durcalist_0)
)

"""Transprom the json dict in python dict"""
parameter_dict = parameters.model_dump()


if __name__ == '__main__':
    with open("./input.nml.json") as f:
        json_in = json.load(f)
        #print(json_in)
        par_in = NeutrinoParameterClass(**json_in)

    # for ii in parameters.__dict__.items():
    #     print(f"{ii[0]} :")
    #     for kk in ii[1].__dict__.items():
    #         print(f"{kk[0]} : {kk[1]}")
