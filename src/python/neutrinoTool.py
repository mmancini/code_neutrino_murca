#!/usr/bin/env python3
###############################################
# Name   : neutrinoTool
# File   : neutrinoTool.py
# Project: Code_Neutrino
# Author : MMancini
# Date   : 24/01/2018
# Function:
#  Merge HDF5 files
###############################################
"""
Module containing classes and object to merge hdf5 files, output of
neutrino code.
"""

__author__    = "Marco Mancini (MM)"
__modifiers__ = ''
__date__      = '25/01/2014'
__version__   = '0.0'

import h5py as h5
import numpy as np
import argparse
#import glob
from pathlib import Path
import sys

# parameters or intial values
directory = 'nu_files'
basename = 'nu_cheb'
outbasename = basename +'_merged'
mode = 'auto'
first = True



class NuValues :
    npts =  0
    ndfinal = 0
    nutype = ''
    rmax = None
    rmin = None
    coeff = None
    def __init__(self,nutype):
        self.nutype = nutype

    def h5write_get(self,pts_group):
         # try :
        subgroup = pts_group[self.nutype]
        self.ndfinal = subgroup.attrs['ndfinal'][0]
        self.rmax = subgroup['enuthrmax'][()]
        self.rmin = subgroup['enuthrmin'][()]
        self.coeff = subgroup['coefficients'][()]
         # except:
         #     print(self.nutype +' sybgroup is not included')
    def __str__(self):
        mystr = self.nutype+'\n'
        mystr += f"{self.npts} {self.ndfinal} \n"
        mystr += f"{self.rmin}\n"
        mystr += f"{self.rmax}\n"
        mystr += f"{self.coeff}\n"
        return mystr

class NuNubar :
    nu = NuValues('nu')
    nubar = NuValues('nu_bar')

    def __init__(self,pts_group) :
        self.nu.h5write_get(pts_group)
        self.nubar.h5write_get(pts_group)
    def __str__(self) :
        return self.nu.__str__()+self.nubar.__str__()


def cmd_line():
    """Command line parser of Merge Tool for Code_Neutrino """
    parser = argparse.ArgumentParser(
        description='Merge Tool for Code_Neutrino',
        )
    parser.add_argument('--basename', metavar='BASENAME', dest='basename',type=str,default=basename,
                        help='Base name of the files to merge.')

    parser.add_argument('--mode',type=str, dest='mode',
                        metavar='MODE',default=mode,
                        choices=['auto','all','list'],
                        help="""How the files are found. If option
                        --list is given, then mode is forced on 'list'""")


    parser.add_argument('--dir', metavar='DIRECTORY',dest='directory',default=directory,
                        help='Directory where file to analyse are stored.')

    parser.add_argument('--out', metavar='OUTBASENAME',dest='outbasename',default=outbasename,
                        help='name of the output file.')

    parser.add_argument('--file', metavar='FILE',dest='filepath',default=None,
                        help='If used the analysys of the FILE is done.')


    parser.add_argument('--action', type=str, default='merge',choices=['merge','search','verify'],
                        dest='action',
                        help="""Different functionalities :\n
                                1) merge : the merge is done from files.
                                2) search : find where a point (defined with the '-tny' option, is .
                                3) verify : Verifies the set of missing points in a file (given by the option
                        '--file') in a range by the '-tny' and '-tny1' options.
""")


    parser.add_argument('-tny', metavar=('T','n','Y'),default=None,nargs=3,dest='TnY0',
                        help="""Point to analyse. If option '--verify'
                        is used then this option is used to give the
                        initial point.""")

    parser.add_argument('-tny1', metavar=('T','n','Y'),default=None,nargs=3,dest='TnY1',
                        help="""With option '--verify', to indicate
                        the last point.""")

    parser.add_argument('--list',metavar='FILESLIST', dest='fileslist',type=str,nargs='+',default=[],
                        help="using '--mode=list' this variable contains the list of file to merge")

    return parser



def Merge(directory,basename,outbasename,mode,filepath,action,TnY0,TnY1,fileslist):


    if filepath is None or action=="verify" : # No a single file

        # scan directory
        p = Path(directory)
        outname = p / Path(outbasename+".h5")

        # mode is changed in "list" when a list of file is given
        if fileslist != [] : mode = "list"


        print(f" Merge Tool for Code_Neutrino\n")
        print(f" Method    : {mode}")
        print(f" Directory : {directory}\n")

        # manage the list of files :
        if mode == 'auto':
            seed =  p / Path(basename+'_0000.h5')

            if not seed.is_file() :
                print(f"\n Error: file {seed} not found !!\n")
                sys.exit(1)
            fin = h5.File(seed,'r')
            nproc = fin['parallel'].attrs['nproc'][0]
            fin.close()
            files = [p/ Path(basename+'_'+jj+'.h5') for jj in  ("{:04d}".format(ii) for ii in range(0,nproc)) ]

        elif mode == 'all' :
            files = sorted(list(p.glob(basename+'_*.h5')))

        elif mode == 'list' :
            if fileslist==[] :
                print(f"""\n Error: With mode "list" you have to specify the list of files to merge with "--list" option""")
                exit(0)
            files = [ Path(ff) for ff in fileslist ]
            print()

        if action == 'merge' :
            fout = h5.File(  outname, "w")

            parameters_is_read = False
            for ifile in files :
                if(ifile == outname) : continue
                try:
                    fin = h5.File(ifile, 'r')

                    print (f" Reading file : {ifile.name}")

                    for gg,vv in fin.items():

                        if gg in ('parallel','metadata') : continue
                        if gg =='parameters' :
                            if parameters_is_read :
                                continue
                            else:
                                parameters_is_read = True

                        try :
                            #print (f"\tCoping {gg}")
                            fin.copy(gg,fout)
                        except:
                            print(f" group {gg} exists in {fout.filename}: it will be overwritten")
                            fout.pop(gg) # deleting old key
                            fin.copy(gg,fout)
                            #raise
                except:
                    print(f" Something strong with file {ifile.name} !!")
                    #raise
                finally:
                    fin.close()

            fout.close()

        elif action == 'search' :
            if TnY0 is None :
                print(r"'action=search' need '-tny' option to be defined")
                exit(0)
            tny_pt = "{0:04d}_{1:04d}_{2:04d}".format(*map(int,TnY0))
            for ifile in list(p.glob(basename+'_*.h5')):
                fin = h5.File(ifile,'r')
                if tny_pt in fin.keys():
                    print( ifile)

        # verify the missing point
        elif action == 'verify' :
            if filepath is None :
                print(r"'action=verify' need options --file ")
                exit(0)
            if TnY0 is None :
                print(r"'action=verify' need '-tny' option to be defined")
                exit(0)
            if TnY1 is None :
                print(r"'action=verify' need '-tny1' option to be defined")
                exit(0)
            start = list(map(int,TnY0))
            end = list(map(int,TnY1))
            for tt in range(start[0],end[0]+1) :
                for nn in range(start[1],end[1]+1) :
                    for yy in range(start[2],end[2]+1) :
                        tny_pt = "{0:04d}_{1:04d}_{2:04d}".format(tt,nn,yy)
                        fin = h5.File(filepath,'r')
                        if tny_pt not in fin.keys():
                            print(tny_pt)


    else:
        # option file is present
        ifile = Path(filepath)
        if not ifile.is_file() :
            print (f"\n Error: file {filepath} not found !!\n")
            sys.exit(1)


        print (f' Analysing file {filepath}')

        fin = h5.File(ifile,'r')

        if TnY0 is None :
            print (fin.keys())
        else:
            print (f'  Extracting point {TnY0} ')
            tny_pt = "{0:04d}_{1:04d}_{2:04d}".format(*map(int,TnY0))
            if tny_pt not in fin.keys():
                print (f'  Point {tny_pt} not in {filepath}')
                exit(0)

            npts = fin['parameters'].attrs['npts'][0]

            group = fin[tny_pt]

            result = NuNubar(group)
            print(result)


if __name__=='__main__':
    parser = cmd_line()
    args = parser.parse_args()

    Merge(**vars(args))
