import json
import collections.abc
import f90nml
from sys import exit
from copy import deepcopy
from parameter_list import parameter_dict


nml_reference = {
    kk: { vv1["name"] : vv1["value"] for _,vv1 in vv.items() } for kk,vv  in parameter_dict.items()
}


nml_json = {
    ### {nmlkey : json_key}
    kk: { vv1["name"] : kk1 for kk1,vv1 in vv.items() } for kk,vv  in parameter_dict.items()
}



def compare_dicts(a,b,filename="",nivel=""):
    """Compare two dicts and return True if they are equal , otherwise
    it exits with an error"""
    aa = set(a.keys())
    bb = set(b.keys())
    if aa==bb:
        return True
    if bb.issubset(aa):
        print(f"\nWARNING : File {filename}:{nivel} do not contain {aa.difference(bb)}\n")
        return True
    else :
        print ('test :',aa)
        print ('ref  :',bb)
        string = f"\nError: file {filename} " if filename is not None else f"\nError:"
        string += f"'ref' is not conform to 'test' is not conform in {nivel}."
        string += f" Symmetric difference: {aa^bb}"
        print(string)
        exit(1)


def compare_dict_deep(reference,test_param,filename=""):
    compare_dicts(reference, test_param, filename)
    for kk,vv in test_param.items():
        compare_dicts(reference[kk],vv, filename=filename, nivel=kk)
    return True



def compare_json(filename):
    with open(filename) as f:
        test_param = json.load(f)
        compare_dict_deep(parameter_dict,test_param)
    return test_param


class Content():

    def __init__(self):
        self.param = deepcopy(parameter_dict)

    def load_nml(self,filename=None):
        with open(filename,'r') as f:
            nml_in = f90nml.read(f).todict()

        # let compare the dict with the parameters
        compare_dict_deep(nml_reference,nml_in,filename)

        for vv,kk in nml_in.items():
            #print(vv)
            # loop on internal keys
            for nml_key,nml_val in kk.items():
                # extract the json key and the value
                json_key = nml_json[vv][nml_key]
                json_entry = self.param[vv][json_key]
                default = json_entry['default']

                # treat here the entries with a choice
                if  "contr" in json_entry and json_entry["contr"] is not None:
                    inv_contr = {v: k for k, v in json_entry["contr"].items()}
                    if nml_val not in inv_contr:
                        print(f"""Error: in {filename}. In {vv} the
                        variable {json_key} has to be value in {inv_contr}.""")
                        exit(1)
                    else:
                        if self.param[vv][json_key]["value"] != inv_contr[nml_val]:
                            self.param[vv][json_key]["value"] = inv_contr[nml_val]
                            print(f"Set {vv}:{json_key} = { inv_contr[nml_val]}")

                elif type(default) == list:
                    if type(nml_val) != list  :
                        if str(nml_val).isnumeric():
                            nml_val = [nml_val for ii in range(len(default))]
                        else:
                            print(f"ERROR: in {filename}.")
                            print(f"In {vv} the variable {json_key} has to be a numeric array or a number.")
                            exit(1)
                else:
                    if self.param[vv][json_key]["value"] != nml_val:
                        self.param[vv][json_key]["value"] = nml_val
                        print(f"Set {vv}:{json_key} = { nml_val}")


    def dump_nml(self,filename=None):
        nml_out = {}
        for kk,vv in self.param.items():
            nml_out[kk] = {}
            for kk1,vv1 in vv.items():
                if "contr" in vv1  and vv1["contr"] is not None:
                    nml_out[kk][vv1["name"]] = vv1["contr"][str(vv1["value"])]
                else:
                    nml_out[kk][vv1["name"]] = vv1["value"]
                    if vv1["value"] is None:
                        print(f'WARNING {kk} : {vv1["name"]} has not default value : you have to set it')

        if not filename.endswith('.nml') and not filename.endswith('.nl'):
            filename += ".nml"

        nml = f90nml.Namelist(nml_out)
        nml.write(filename,force=True)

        #print(f"The file : {filename} has been created")
        return filename



    def load_json(self,json_in,filename=None,verbose=False):
        if verbose : print(2*"\n")
        alldefault = True

        compare_dict_deep(parameter_dict,json_in,filename)
        for kk,vv in json_in.items():
            # loop on internal keys
            for json_key,json_entry in vv.items():

                # extract the json value and the value
                json_val = json_entry["value"]
                default = self.param[kk][json_key]['default']

                # treat here the entries with a choice
                if "contr" in json_entry and json_entry["contr"]  :
                    if str(json_val) not in json_entry["contr"] and json_val is not None:
                        print(f"""ERROR: in {filename}. In {kk} the
                        variable {json_key} has to be in {json_entry["contr"]}.""")
                        exit(1)
                if type(default) == list:
                    if type(json_val) != list  :
                        if str(json_val).isnumeric():
                            json_val = [json_val for ii in range(len(default))]
                        else:
                            print(f"""ERROR: in {filename}. In {kk} the
                            variable {json_key} has to be a numeric array or a number.""")
                            exit(1)
                if  json_val != default :
                    alldefault = False
                    self.param[kk][json_key]['value'] = json_val
                    if(verbose) : print(f"Set {kk}:{json_key} = {json_val}")
        if alldefault and verbose: print("All default values")

    def load_json_file(self,filename,verbose=False):
        with open(filename,'r') as f:
            json_in = json.load(f)
        self.load_json(json_in,filename,verbose)


    def dump_json(self,filename=None):
        if not filename.endswith('.json') :
            filename += ".json"
        json_out = json.dumps(self.param,indent=4)

        with open(filename,'w') as f:
            f.write(json_out)
        #print(f"The file : {filename} has been created")
        return filename




if __name__ == "__main__":
    content = Content()
    content.load_json_file("input.nml.json",verbose=True)
    # content.load_nml("input.nl")
    # content.dump_nml("input2.nl")
    #content.dump_json("input3.json")
