"""
    Module containg the parser for neutrino code
"""

from argparse import ArgumentParser


class NeutrinoParser(ArgumentParser):
    def __init__(self, *args, **kwargs):
        #print ("NeutrinoParser")
        super(NeutrinoParser, self).__init__(*args, **kwargs)



__parser = NeutrinoParser(prog="neutrino",
                                 description='neutrino_murca code: ADD-INFOS ICI')
__parser.add_argument('-n','--nproc', metavar='np', type=int,
                      dest='nproc',
                      required=False,
                      default=1,
                      help='Number of MPI processus to launch')

__parser.add_argument('-ct','--create_template',
                      dest='create_template', action='store_true',
                      help='Create json template')

__parser.add_argument('-c','--compute',
                      dest='compute', action='store_true',
                      default=True,
                      help='Create json template')


__parser.add_argument('-in','--input',
                      dest='input_json_file', type=str,
                      required=False,
                      #"template_input.json",
                      help='Input file name or name of created template')


__parser.add_argument('-j2n','--convert_j',
                      dest='convert_file_j2n', type=str,
                      help='Convert json to nml file')
__parser.add_argument('-n2j','--convert_n',
                      dest='convert_file_n2j', type=str,
                      help='Convert nml to json file')

__parser.add_argument('--info',
                      dest='getinfo', action='store_true',
                      default=False,
                      help='Print information on input variables')



def get_commands():
    # getting command line
    commands = __parser.parse_args()

    # adding help to command
    commands.print_help = __parser.print_help

    # controls

    return commands
