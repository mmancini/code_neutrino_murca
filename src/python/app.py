#!/usr/bin/env python

# app.py
import eel
import json
import neutrino_launcher as neutlau
from datetime import datetime
from manage_input import Content
import os

# 'web' is the directory which contains the files HTML, CSS and JavaScript
dirname = os.path.dirname(__file__)
path = os.path.join(dirname, "src/python/web")
print(path)
eel.init(path)

global content

content = Content()


def print_pretty(par_dict):
    for kk,vv in par_dict.items():
        print(kk)
        for kk1,vv1 in vv.items():
            if vv1['value'] != vv1['default']:
                print("  ",kk1,vv1['value'], vv1['default'])


@eel.expose
def get_parameters():
    # Ritorna i dizionari di parametri come stringhe JSON
    return {
        "inputlist": json.dumps(content.param["inputlist"]),
        "murcalist": json.dumps(content.param["murcalist"]),
        "durcalist": json.dumps(content.param["durcalist"]),
    }

@eel.expose
def save_parameters(updated_params,
                    filename="updated_parameters.json",
                    verbose=False):
    # Salva i parametri aggiornati nel file JSON
    content.load_json(updated_params,filename=filename,verbose=verbose)
    print(f"Saving parameters on file {filename}")
    content.dump_json(filename)



@eel.expose
def run_application(filename=None,nproc=1,nthreads=None,verbose=False):
    # params = Content()
    neutlau.launch_program(content,filename,nproc,nthreads,verbose=False)



def close_callback(route, websockets):
    if not websockets:
        print('Bye!')
        #exit()

# @eel.expose
# def process_file(file_path):
#     try:
#         content.load_json_file(file_path)
#         print_pretty(content)

#         return content
#     except Exception as e:
#         return f"Error uploading the file : {str(e)}"

@eel.expose  # Expose the function to JavaScript
def quit_app():
    exit()  # Quit the app




eel.start('index.html', size=(500, 900),
          close_callback=close_callback,
          # mode='chrome-app',
          host='localhost',
          disable_cache=True,
          port=8001)
