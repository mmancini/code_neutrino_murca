#!/usr/bin/env python3

"""Wrapper for neutrino code.
It permits to use json input file and to create a template for a
input. The fortran binary is launched with the given number of processus
"""


import subprocess
import sys
import errno
import os
from pathlib import Path

from parser_neutrino import get_commands
from manage_input import Content



# path of the binary. If None the binary will be searched in the
# canonical installation direcotry (../../BUILD/neutrino)
DISTRIBUTION_PATH=None

if DISTRIBUTION_PATH is None:
    DISTRIBUTION_PATH = Path(sys.argv[0]).resolve().parent.parent.parent / "BUILD"

executable = DISTRIBUTION_PATH / "neutrino"





def launch_program(params,input_json_file,nproc=1,nthreads=None,verbose=False):


    if nthreads is None: nthreads = "auto"

    print (f"""
     Launching {sys.argv[0]}
     input file : {input_json_file}
     nproc      : {nproc}
     nthreads   : {nthreads}
    """)

    # read json input file
    params.load_json_file(input_json_file,verbose=True)

    # create input file (nml)
    input_nml_file = input_json_file.replace(".json",".nml")

    # file conversion
    input_nml_file = params.dump_nml(input_nml_file)


    # Test is executable is there
    if not Path(executable).exists() :
        print (f"{os.getcwd()}")
        print (f"Neutrino binary does not found : please control {executable} !!")
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), executable)


    exec_line = f'mpirun'
    if nthreads != 'auto':
        exec_line += f' -x OMP_NUM_THREADS={nthreads}'
    exec_line += f' --oversubscribe -n {nproc} {executable} -in {input_nml_file}'

    print (100*"-")

    print (f"""
     Launching fortran binary : {exec_line}
    """)

    process = subprocess.run(exec_line.split(" "),
                             stdout=None,
                             universal_newlines=True,
                             check=True)





def prepare_launch(**kwargs):


    # parsing line commmands
    commands = get_commands()

    # default parameter list
    params = Content()


    # get number of MPI processus
    nproc = commands.nproc
    input_json_file = commands.input_json_file


    # Template creation
    if commands.create_template :
        if commands.input_json_file is None:
            commands.input_json_file = "template.json"
        template = params.dump_json(commands.input_json_file)
        print(f"Created a template for input file (json) : {template}")
        sys.exit(0)

    elif commands.convert_file_j2n:
        # test input file (json)
        if not Path(commands.convert_file_j2n).exists() :
            print(f"Error : no input file found.\n")
            commands.print_help()
            sys.exit(0)
        print(f"Convert json file : {commands.convert_file_j2n} to nml")
        params.load_json_file(commands.convert_file_j2n)
        fileout = params.dump_nml(commands.convert_file_j2n)
        print(f"\nConverted {commands.convert_file_j2n} to {fileout}")
        sys.exit(0)

    elif commands.convert_file_n2j :
        if not Path(commands.convert_file_n2j).exists() :
            print(f"Error : no input file found.\n")
            commands.print_help()
            sys.exit(0)
        print(f"Convert nml file : {commands.convert_file_n2j} to json")
        params.load_nml(commands.convert_file_n2j)
        fileout = params.dump_json(commands.convert_file_n2j)
        print(f"\nConverted {commands.convert_file_n2j} to {fileout}")
        sys.exit(0)


    elif commands.getinfo :
        params.help()


    elif commands.compute :
        # test input file (json)
        if input_json_file is not None :
            if not Path(input_json_file).exists():
                print(f"Error : no input file found.\n")
                commands.print_help()
                sys.exit(0)
            launch_program(params,input_json_file,commands.nproc)
        else:
            print(f"Error : no input file found.\n")
            commands.print_help()
            sys.exit(0)



if __name__ == "__main__":
    prepare_launch()
