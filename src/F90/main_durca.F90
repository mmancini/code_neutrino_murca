!*********************************************************
! This routine calls the calculation of Durca processes over
! a certain range of temperatues, densities and electron fractions
! from the EoS table.
!*********************************************************

module main_durca_m
 use EOS_COMPOSE
 use parameter_durca
 use apr_eos
 use skyrmeparameter
 use virial_eos


 implicit none

 private

 public :: main_durca_process, test_integrations_durca_process

contains

 subroutine main_durca_process

  use m_calculate, only : write_chebichev

  !mf :  which type of (effective) masses and chemical potentials
  ! mf = 1: take mean field masses/chemical potentials
  ! mf = 2: take free masses/chemical potentials
  integer,parameter :: nrepeat=120

  if(rank==0)then
    write(*,*) repeat('_',nrepeat)

    write(*,*) 'Number of points in temperature',t_pts
    write(*,*) 'Number of points in density',nb_pts
    write(*,*) 'Number of points in charge fraction',y_pts

    ! call set_mf_cv()
    ! call read_durcalist_nml()
    ! call print_durcalist_nml()
  endif

  ! call distribute_variables_mpi_cv()
  ! call distribute_variables_mpi_durca_cv()

  if(eos_type == 1) then
    call set_skyrme_parameters()
  else if(eos_type==2) then ! APR equation of state
    call set_apr_parameters()
  else if(eos_type==3) then
    call set_virial_parameters()
  end if
  call set_enumin_max_cv()

  call write_chebichev(mf,pts_pol,enumin,enumax)

  ! processes
  ! inp : n+ nu -> p + e (ipn is inverse)
  ! inp_eplus: n + e^+ + nu -> p (ipn_eplus is inverse)
  ! inp_nubar: n -> p + e + nubar (ipn_nubar is inverse)
  ! inp_nubar_eplus: n + e^+ -> nubar + p (ipn_nubar_eplus is inverse)


 end subroutine main_durca_process

 !*********************************************************
 ! This routine contains a certain number of tests of the
 ! intermediate steps in the integration of the Durca process
 ! for one given (anti-) neutrino energy and one given
 ! temperatue, density and electron fractions
 ! from the EoS table.
 ! This routine is not parallel for the moment
 !*********************************************************

 subroutine test_integrations_durca_process

  use LindhardFunction, only : determine_gamdecay,calculate_lindhard_width

  use integration_densities, only :    fermi_12


  !   use asym, only: init_full_rpa
  use leptons, only: set_lepton_sign, determine_q0min_and_q0max_leptons

  implicit none


  integer :: i,j,k
  real(dp) :: mu1,mu2,m1,m2
  real(dp) :: q0min,q0max,muq0,eta
  real(dp) :: mub,muq,munu,muel,zn,zp
  character ( len = 10 )  :: nom
  character ( len = 100 ) :: type
  integer :: which_leptons,lepton_sign(3)
  real(dp) :: un,up,gamdecay
  real(dp) :: nneutron, nproton,muproton
  real(dp):: nptest,nntest,correct
  real(dp), parameter :: convertmevg = 1.782661e12_dp


  write(*,*) 'Number of points in temperature',t_pts
  write(*,*) 'Number of points in density',nb_pts
  write(*,*) 'Number of points in charge fraction',y_pts


  ! call set_mf_cv()
  ! call read_durcalist_nml()
  ! call print_durcalist_nml()

  if(eos_type == 1) then
    call set_skyrme_parameters()
  else if(eos_type==2) then ! APR equation of state
    call set_apr_parameters()
  else if(eos_type==3) then
    call set_virial_parameters()
  end if



  ! processes
  ! inp : n+ nu -> p + e (ipn is inverse)
  ! inp_eplus: n + e^+ + nu -> p (ipn_eplus is inverse)
  ! inp_nubar: n -> p + e + nubar (ipn_nubar is inverse)
  ! inp_nubar_eplus: n + e^+ -> nubar + p (ipn_nubar_eplus is inverse)


  if(.not.cheb) then
    write(*,*) 'Extracting from values of T, nb, yq'
    call set_temperature_cv()
    call set_density_cv()
    call set_electron_fraction_cv()
    write(*,*)

    call determine_gamdecay(density,temperature,gamdecay)
    write(*,*) 'calculate for width (MeV)', gamdecay

    call interpolate_eos_compose(temperature,density,electron_fraction,&
      m1,m2,mu1,mu2,muq,mub,muel,nneutron,nproton,i,j,k)

    print '(a,i6,i12,3i6)','--------------INTER ----------------', i,j,k
    muproton = muq + mub

    munu = muq + muel

  else
    write(*,*) 'which point of the EoS table in T,nb,yq?'
    read(*,*) i,j,k
    call set_temperature_cv(temp(i))
    call set_density_cv(nb(j))
    call set_electron_fraction_cv(yq(k))
    muproton = mup(i,j,k)
    mub = mun(i,j,k)
    muel = mue(i,j,k)
    munu = mul(i,j,k)
    mu1 = mupeff(i,j,k)
    mu2 = muneff(i,j,k)
    m1 = mpeff(i,j,k)
    m2 = mneff(i,j,k)
  end if
  write(*,*) 'Density', density,density*mass_neutron*convertmevg*1e-13_dp
  write(*,*) 'Temperature', temperature
  write(*,*) 'Charge fraction', electron_fraction


  write(*,*) tab, tab,'Extracted proton/neutron masses and chemical potential', m1,m2
  write(*,*) tab, tab,'Extracted proton/neutron chemical potentials',mu1,mu2
  write(*,*) tab, tab,'Extracted electron and neutrino chemical potentials',muel,munu,muel/temperature,-muel + munu
  write(*,*) tab, tab,'Extracted nucleon potentials p/n',up,un,un-up,un-up-(mass_neutron-mass_proton) + m2-m1


  if(mf.eq.1) then
    un = mub - mu2
    up = muproton - mu1
  else if (mf.eq.2) then
    mu1 = muproton
    mu2 = mub
    m1 = mass_proton
    m2 = mass_neutron
    up = 0._dp
    un = 0._dp
  else
    call set_temperature_cv(7._dp)
    call set_density_cv(2.e13_dp/(convertmevg*mass_neutron))
    call set_electron_fraction_cv(0.05_dp)

    up = 30.7_dp
    un = 40.3_dp
    m1 = 892.42_dp
    m2 = 893.72_dp
    mu1 = -43.3_dp + mass_proton -up
    mu2 = -0.68_dp + mass_neutron -un
    munu = 4.5_dp
  end if
  write(*,*) 'nucleon potentials p/n'
  write(*,*) up,un,un-up,un-up-(mass_neutron-mass_proton) + m2-m1
  if(eos_type==1) then
    write(*,*) 'new definition of skyrme potentials with difference of effective masses'
    write(*,*) up +mass_proton - m1,un + mass_neutron - m2
  end if
  write(*,*) 'qvalue, q0/temp'
  write(*,*) m2-m1+un-up,(m2-m1+mu1-mu2)/temperature,1/(exp((m2-m1+mu1-mu2)/temperature) - 1)
  write(*,*) 'free values: qvalue, q0/temp'
  write(*,*) mass_neutron-mass_proton,(mass_neutron-mass_proton+muproton-mub)/temperature,&
    1/(exp((mass_neutron-mass_proton+muproton-mub)/temperature) - 1)

  call set_omega_cv()

  blow = -min(2._dp,5*temperature)
  bup = -blow

  if(xn(i,j,k).eq.-1._dp) then ! information about neutron and proton densities does not exist
    nneutron = density*(1-electron_fraction)
    nproton = density*electron_fraction
  else
    nneutron = xn(i,j,k)*density
    nproton = xp(i,j,k)*density
    write(*,*) 'nproton',nproton,density*electron_fraction
  end if

  if(eos_type == 1) then

    if(mf.eq.1) then

      !**********************************************************************
      ! possibility to check whether the Skyrme values coincide with the values
      ! read from the EoS table, uncomment if necessary
      !
      !call check_skyrme_values(m1,m2,mu1,mu2,nneutron,nproton,temperature,electron_fraction,density,mass_neutron,mass_proton)
      !***********************************************************************

      !**********************************************************************
      ! possibility to check whether RPA remains stable, uncomment if necessary
      !call check_stability
      !**********************************************************************

      !***************************************************************
      ! check beta equilibrium, uncomment if necessary
      !***************************************************************
      !!$     do k = 1,30
      !!$        call check_beta_equilibrium(temperature,muel,omega,nneutron,nproton,&
      !!$             m1,m2,mu1,mu2,munu,up,un)
      !!$     end do
      !!$     stop

      eta = (mu1-m1)/temperature
      nptest = (m1*temperature)**(3._dp/2)*sqrt(2._dp)/pi**2*fermi_12(eta)/(hbarc**3)
      eta = (mu2-m2)/temperature
      nntest = (m2*temperature)**(3._dp/2)*sqrt(2._dp)/pi**2*fermi_12(eta)/(hbarc**3)
      write(*,*) 'test for densities, Adrianas approximation n/p ', nntest,nptest
      ! calculate densities with the free parameters (not consistent with the EoS)
      eta = (muproton-mass_proton)/temperature
      nptest = (mass_proton*temperature)**(3._dp/2)*sqrt(2._dp)/pi**2*fermi_12(eta)/(hbarc**3)
      eta = (mub-mass_neutron)/temperature
      nntest = (mass_neutron*temperature)**(3._dp/2)*sqrt(2._dp)/pi**2*fermi_12(eta)/(hbarc**3)
      write(*,*) 'test for densities with free values ', nntest,nptest
    end if
  else if(eos_type==2) then ! APR equation of state

    write(*,*) 'symmetric matter parameters', nneutron+nproton,nproton/(nneutron+nproton)
    write(*,*) 'APR: calculate for proton/neutron masses'
    write(*,*) m1,m2,m1/mass_proton,m2/mass_neutron,mass_proton-m1,mass_neutron-m2
    write(*,*) 'Effective masses read from table at neighbouring point with i,j,k = ',i,j,k
    write(*,*) 'are for proton/neutron', mpeff(i,j,k),mneff(i,j,k)
  else if(eos_type==3) then
    zn = exp((mub-mass_neutron)/temperature)
    zp = exp((muproton-mass_proton)/temperature)
    call determine_zn_zp(nneutron,nproton,zn,zp,temperature,mass_neutron)
    call determine_en_ep(nneutron,nproton,zn,zp,temperature,mass_neutron,un,up)
    write(*,*) 'en,ep',un,up,un-up
    mu1 = muproton - up
    mu2 = mub - un
    m1 = mass_proton
    m2 = mass_neutron

    write(*,*) 'effective chemical potentials', mu1,mu2

  end if

  muq0  = muel - munu
  write(*,*) 'calculating for neutron and proton densities', nneutron,nproton
  write(*,*) 'fractions', xn(i,j,k),xp(i,j,k)
  write(*,*) 'Neutrino energy', omega


  !   ! if testing full RPA, need initialising here (uncomment)
  !   nom = "SLY4"
  !   type = "NONE"
  !   call init_full_rpa(nom,type)

  ! Determine reactions to consider for the test
  write(*,*) 'Which reactions to consider for test of Durca?'
  write(*,*) tab, '1: Electron capture and inverse, i.e. electrons and neutrinos'
  write(*,*) tab, '2: Positron capture and inverse, i.e. positrons and anti-neutrinos'
  write(*,*) tab, '3: Neutron decay and inverse, i.e. electrons and anti-neutrinos'
  write(*,*) tab, '4: Proton decay and inverse, i.e. positrons and neutrinos'
  read(*,*) which_leptons
  write(*,*) tab, "which_leptons ->", which_leptons


  call determine_q0min_and_q0max_leptons(which_leptons,temperature,omega,muel,munu,q0min,q0max)
  write(*,*) 'q0min and q0max (MeV) for chosen process', q0min,q0max
  call set_lepton_sign(which_leptons,lepton_sign)
  write(*,*) 'Signs for computing charged lepton energy for which_leptons = ', which_leptons
  write(*,*) lepton_sign

  !********************************************************************
  ! some tests follow of different stages of integration
  ! feel free to (un)comment the desired tests
  !********************************************************************

  call test_lindhard_width(m1,m2,mu1,mu2,nproton,nneutron)

  call plot_widths
  ! I: plot the Lindhard function and several other functions as function of q
  ! for the above given q0 (manual entry)

  call plot_lindhard_function_q(omega,m1,m2,mu1,mu2,muel,munu,up,un,&
    nproton,nneutron,gamdecay,q0min,q0max,lepton_sign)

!!$  ! II: plot the Lindhard function as function of q0 (and q)
!!$
!!$  call plot_lindhard_function_q0(omega,m1,m2,mu1,mu2,muel,munu,gamdecay,&
!!$    q0min,q0max,lepton_sign)
!!$  
!!$
!!$  ! III: comparison with the results of Hernandez et al 1999
!!$
!!$  call compare_with_hernandez(omega,m1,m2,mu1,mu2,muel,munu,up,un,&
!!$    nproton,nneutron,gamdecay,q0min,q0max)

  ! IV: test the integration over q and plot the result as f(q0)

  call plot_integration_over_q(omega,m1,m2,mu1,mu2,muel,munu,&
    gamdecay,q0min,q0max,lepton_sign,nproton,nneutron)

  ! V: Complete Durca integration using the different approximations
  ! for the given neutrino energy omega
  ! (as function of neutrino energy -> full Durca calculations)

  call check_durca_full_integration(omega,m1,m2,mu1,mu2,muel,munu,nproton,nneutron,gamdecay)

 end subroutine test_integrations_durca_process

!**************************************************************
 subroutine test_lindhard_width(m1,m2,mu1,mu2,nproton,nneutron)

  use LindhardFunction, only: calculate_lindhard_width,&   
    &                         calculate_lindhard_width_0,gamma_1,gamma_2   



  real(dp), intent(in) :: m1,m2,mu1,mu2
  real(dp), intent(in) :: nneutron,nproton

  real(dp) :: lim_width,gn,gp

  real(dp) :: q0,q

  q0 = 1._dp
  q = 170._dp
  gp = gamma_1(temperature,m1,m2,mu1,mu2,nproton,nneutron)
  gn = gamma_2(temperature,m1,m2,mu1,mu2,nproton,nneutron)
  call calculate_lindhard_width(q0,q,m1,m2,mu1,mu2,temperature,gn,gp,lim_width)
  write(*,*) 'Resulting Lindhard', lim_width

end subroutine test_lindhard_width

 
 ! Plot the different prescriptions for the quasi-particle width as
 ! function of temperature and density and energy

 subroutine plot_widths
   
    use LindhardFunction, only: determine_gamdecay,&
    &                         gamma_1,gamma_2,width_frequency   
   
    real(dp) :: nmin = 1.e-4_dp,nmax = 1._dp,tmin = 0.2,tmax = 50
    real(dp) :: omin = -500,omax = 500

    real(dp) :: density,temperature,electron_fraction
    real(dp) :: m1,m2,mu1,mu2,muq,mub,muel,nneutron,nproton
    real(dp) :: omega,gamdecay,gn,gp,an,ap
    integer :: ii,jj,kk,in,it,io
    integer :: pkten = 50,pktet = 50,pkteo = 50


    open(50,file='testwidth.d', status='unknown')

    do it = 0,pktet
       temperature = tmin + it*(tmax-tmin)/pktet
       do in = 0,pkten
          density = nmin + in*(nmax-nmin)/pkten
          call interpolate_eos_compose(temperature,density,electron_fraction,&
      m1,m2,mu1,mu2,muq,mub,muel,nneutron,nproton,ii,jj,kk)
          call determine_gamdecay(density,temperature,gamdecay)
          gn = gamma_2(temperature,m1,m2,mu1,mu2,nproton,nneutron)
          gp = gamma_1(temperature,m1,m2,mu1,mu2,nproton,nneutron)
          write(50,*) temperature,density,gamdecay,gn,gp
       end do
       write(50,*)
    end do
    close(50)
    open(50,file='testwidth_omega.d', status='unknown')

    ! test for T = 1 MeV and saturation density
    temperature = 1._dp
    density = 0.16_dp
    call interpolate_eos_compose(temperature,density,electron_fraction,&
      m1,m2,mu1,mu2,muq,mub,muel,nneutron,nproton,ii,jj,kk)
    call determine_gamdecay(density,temperature,gamdecay)
    gn = gamma_2(temperature,m1,m2,mu1,mu2,nproton,nneutron)
    gp = gamma_1(temperature,m1,m2,mu1,mu2,nproton,nneutron)
    
    do io = 0,pkteo
       omega = omin + io*(omax-omin)/pkteo
       an = width_frequency(gn,temperature,omega)
       ap = width_frequency(gp,temperature,omega)
       write(50,*) omega,gamdecay,an,ap
    end do
    close(50)
  end subroutine plot_widths
 
 ! Plot Lindhard function as function of q for a given q0


 subroutine plot_lindhard_function_q(omega,m1,m2,mu1,mu2,muel,munu,up,un,&
   nproton,nneutron,gamdecay,q0min_reac,q0max_reac,lepton_sign)

  use LindhardFunction, only: calculate_lindhard_imaginary,&
    &                         lindhard_imaginary_my, lindhard_general,&
    &                         lindhard_general_decay,&
    &                         lindhard_imaginary,&
    &                         calculate_lindhard_width,&   
    &                         calculate_lindhard_width_0,gamma_1,gamma_2   



  real(dp), intent(in) :: q0min_reac,q0max_reac,m1,m2,mu1,mu2,muel,munu,omega
  real(dp), intent(in) :: up,un,nneutron,nproton,gamdecay
  integer, intent(in) :: lepton_sign(3)

  real(dp) :: lim,lim_my,w,lim_width,lim_test
  real(dp) :: rpaa,rpav,pil,pit,q2,in(3),gammai(2)
  real(dp) :: qt2,beta,pilneu,pitneu,j1,j2,j3,gn,gp
  complex(dp) :: j3_nonrel,j3_nonrel_decay,j3_nonrel_decay_imag

  real(dp) :: qmin,qmax,q0,q
  integer :: nq,pts_q = 100,ngamma = 2
  real(dp) :: ee,pe,q0t

  write(*,*) '********************************************************'
  write(*,*) 'Plot Lindhard function as function of q for given q0 ***'
  write(*,*) '********************************************************'

  write(*,*) 'Calculate for which q0 (MeV)?'
  read(*,*) q0
  q0t = q0 + mu2-mu1
  gammai(1) = gamma_1(temperature,m1,m2,mu1,mu2,nproton,nneutron)
  gammai(2) = gamma_2(temperature,m1,m2,mu1,mu2,nproton,nneutron)
  write(*,*) 'gamdecay= ',gamdecay
  ee = lepton_sign(1)*q0 + lepton_sign(2)*omega + lepton_sign(3)*(muel - munu)
  pe = ee**2 - me**2
  if(pe < 0._dp) pe = 0._dp
  pe = sqrt(pe)
  qmin = abs(pe-omega)
  qmax = pe + omega
  write(*,*) 'qmin, qmax = ', qmin,qmax
  open(21,file='testq.d', status='unknown')
  do nq = 0,pts_q
!  do nq = 48,48
    q = qmin + nq*(qmax-qmin)/pts_q
    j3_nonrel = lindhard_general(q0,q,m1,m2,mu1,mu2,temperature)
    j3_nonrel_decay = lindhard_general_decay(q0,q,m1,m2,mu1,mu2,temperature,gamdecay)
    lim = lindhard_imaginary(q0,q,m1,m2,mu1,mu2,temperature)
    lim_my = lindhard_imaginary_my(q0,q,m1,m2,mu1,mu2,temperature)
    if(gn*gp.eq.0._dp) then
       call calculate_lindhard_width_0(q0,q,m1,m2,mu1,mu2,temperature,gammai(1),gammai(2),lim_width)
    else
       call calculate_lindhard_width(q0,q,m1,m2,mu1,mu2,temperature,gammai(1),gammai(2),lim_width)
    end if
    call calculate_lindhard_imaginary(q0,q,m1,m2,mu1,mu2,temperature,lim_test,lim_test,gammai(1),gammai(2))
    w = q0 -( mu1-mu2 + up -un + m2 -m1)

    q2 = q0**2 - q**2
    qt2 = q0t**2 - q**2
    beta = 1 + (m1**2-m2**2)/qt2
    j1 = in(1)
    j2 = (m1**2 - beta*qt2/2)*in(1)
    j3 = q0t*in(2) + in(3)
    pil = -2*q2/q**2*(q0t*in(2) + in(3) - ((m2-m1)**2 - qt2)*in(1)/4)
    pit = (pil + in(1)*( (m2-m1)**2 - 2*m2*m1 - qt2))/2
    pilneu = q2/q**2*(j2 - 2*j3 - m1*m2*j1)
    pitneu = q2/(2*q**2)*( - 2*j3 + q**2/q2*(2*j2 - 4*m1*m2*j1) + j2 - m1*m2*j1)
    write(21,*) q0,q,lim,lim_my,lim_width,lim_test,&
         real(j3_nonrel),aimag(j3_nonrel),&
         real(j3_nonrel_decay),aimag(j3_nonrel_decay)

  end do

  close(21)
 
end subroutine plot_lindhard_function_q





 subroutine plot_lindhard_function_q0(omega,m1,m2,mu1,mu2,muel,munu,gamdecay,&
   q0min_reac,q0max_reac,lepton_sign)

  use LindhardFunction, only: lindhard_imaginary,lindhard_imaginary_my,&
    &                      lindhard_general_decay,lindhard_general_decay_imag,&
    &                      calculate_lindhard_width,gamma_1,gamma_2,&   
    &                      calculate_lindhard_width_0   

  real(dp), intent(in) :: q0min_reac,q0max_reac,m1,m2,mu1,mu2,muel,munu,omega,gamdecay
  integer, intent(in) :: lepton_sign(3)

  real(dp) :: q0min,q0max,qmin,qmax,q0
  character :: yes_or_no
  integer :: nq,nq0,pts_q0 = 100,pts_q = 10
  real(dp) :: ee,pe,q,lim,lim_my,valueq,lim_width,gn,gp

  complex(dp) :: j3_nonrel,j3_nonrel_decay_imag

  write(*,*) '********************************************************'
  write(*,*) 'Plot Lindhard function as function of q0 for given q ***'
  write(*,*) '********************************************************'
!  write(*,*) 'How many points in q? '
  !  read(*,*) pts_q

   gn = gamma_2(temperature)
   gp = gamma_1(temperature)
  
  if(pts_q.le.1) then
    write(*,*) 'Which value for q? '
    write(*,*) 'Give a value between 0 and 1 to be scaled between qmin and qmax'
    read(*,*) valueq
    if((valueq.lt.0.).or.(valueq.gt.1.)) then
      write(*,*) 'Give a value between 0 and 1'
      read(*,*) valueq
    end if
    pts_q = 0 ! to avoid doing the loop on q
    write(*,*) 'Chosen value', valueq
  end if
  open(21,file='testlindhard.d', status='unknown')

  write(21,*) '# q0 (MeV)   q(MeV)   Im_Durca (MeV^2)  Im_Durca_my (MeV^2) Im Durca_width (MeV^2)',&
  'Im_Durca_width_one_particle (MeV^2)'
  write(21,*) '# and real and imaginary part of Lindhard function with quasi-particle width'
!  write(*,*) 'Plot Lindhard function: do you want to change minimum and maximum value for q0 with respect to chosen process (y/n)?'
!  read(*,*) yes_or_no
  yes_or_no = 'n'
  if(yes_or_no.eq.'y') then
    write(*,*) 'q0min?'
    read(*,*) q0min
    write(*,*) 'q0max?'
    read(*,*) q0max
  else
    q0min = q0min_reac
    q0max = q0max_reac
  end if
  q0loop: do nq0 = 0,pts_q0
    q0 = q0min + nq0*(q0max-q0min)/pts_q0
    ee = lepton_sign(1)*q0 + lepton_sign(2)*omega + lepton_sign(3)*(muel - munu)
    pe = ee**2 - me**2
    if(pe < 0._dp) cycle q0loop
    pe = sqrt(pe)
    qmin = abs(pe-omega)
    qmax = pe + omega

    qloop: do nq = 0,pts_q
      if(pts_q.le.1) then
        q = qmin + valueq*(qmax-qmin)
      else
        q = qmin + nq*(qmax-qmin)/pts_q
      end if

      j3_nonrel = lindhard_general_decay(q0,q,m1,m2,mu1,mu2,temperature,gamdecay)
      j3_nonrel_decay_imag = lindhard_general_decay_imag(q0,q,m1,m2,mu1,mu2,temperature,gamdecay)
      lim = lindhard_imaginary(q0,q,m1,m2,mu1,mu2,temperature)
      lim_my = lindhard_imaginary_my(q0,q,m1,m2,mu1,mu2,temperature)
      if(gn*gp.eq.0._dp) then
         call calculate_lindhard_width_0(q0,q,m1,m2,mu1,mu2,temperature,gn,gp,lim_width)
      else
         call calculate_lindhard_width(q0,q,m1,m2,mu1,mu2,temperature,gn,gp,lim_width)
      end if
      write(21,*) q0,q,lim,lim_my,lim_width,&
           real(j3_nonrel),aimag(j3_nonrel),&
        real(j3_nonrel_decay_imag),aimag(j3_nonrel_decay_imag)
      if(pts_q.le.1) exit qloop
    end do qloop
    if(pts_q.gt.1) write(21,'()')
  end do q0loop
  close(21)
  ! This is the same as above but with inverse arguments for protons and neutrons
  open(21,file='testlindhardinv.d', status='unknown')
  q0loop1: do nq0 = 0,pts_q0
    q0 = q0min + nq0*(q0max-q0min)/pts_q0
    ee = lepton_sign(1)*q0 + lepton_sign(2)*omega + lepton_sign(3)*(muel - munu)
    pe = ee**2 - me**2
    if(pe < 0._dp) cycle q0loop1
    pe = sqrt(pe)
    qmin = abs(pe-omega)
    qmax = pe + omega
    qloop1: do nq = 0,pts_q
      if(pts_q.le.1) then
        q = qmin + valueq*(qmax-qmin)
      else
        q = qmin + nq*(qmax-qmin)/pts_q
      end if
      j3_nonrel = lindhard_general_decay(q0,q,m1,m2,mu1,mu2,temperature,gamdecay)
      j3_nonrel_decay_imag = lindhard_general_decay_imag(q0,q,m1,m2,mu1,mu2,temperature,gamdecay)
      lim = lindhard_imaginary(q0,q,m2,m1,mu2,mu1,temperature)
      lim_my = lindhard_imaginary_my(q0,q,m2,m1,mu2,mu1,temperature)
      if(gn*gp.eq.0._dp) then
         call calculate_lindhard_width_0(q0,q,m2,m1,mu2,mu1,temperature,gn,gp,lim_width)
      else
         call calculate_lindhard_width(q0,q,m2,m1,mu2,mu1,temperature,gn,gp,lim_width)
      end if
      write(21,*) q0,q,lim,lim_my,lim_width,&
           real(j3_nonrel),aimag(j3_nonrel),&
        real(j3_nonrel_decay_imag),aimag(j3_nonrel_decay_imag)
      if(pts_q.le.1) exit qloop1
    end do qloop1
    if(pts_q.gt.1) write(21,'()')
  end do q0loop1
  close(21)
 end subroutine plot_lindhard_function_q0

 ! III: comparison with the results of Hernandez et al 1999

 subroutine compare_with_hernandez(omega,m1,m2,mu1,mu2,muel,munu,up,un,&
   nproton,nneutron,gamdecay,q0min_reac,q0max_reac)

  use LindhardFunction

  real(dp), intent(in) :: q0min_reac,q0max_reac,m1,m2,mu1,mu2,muel,munu,omega
  real(dp), intent(in) :: nproton,nneutron,up,un,gamdecay

  integer :: nq0,pts_q0 = 100
  real(dp) :: kf,q,q0min,q0max,tempher,wurzel,q0,q0t
  real(dp) :: rpaa,rpav,lim,lim_my
  character :: yes_or_no
  complex(dp) :: j3_nonrel,j3_nonrel_decay

  write(*,*) '***********************************************'
  write(*,*) 'Compare with Hernandez 99 paper:'
  write(*,*) '***********************************************'
  kf = (3*pi**2*density/2)**(1._dp/3)*hbarc
  q = kf*0.5
  write(*,*) 'Comparison with Hernandez: calculate for q', q
  write(*,*) 'Do you want to change minimum and maximum value for q0 with respect to chosen process (y/n)?'
  read(*,*) yes_or_no
  if(yes_or_no.eq.'y') then
    write(*,*) 'q0min?'
    read(*,*) q0min
    write(*,*) 'q0max?'
    read(*,*) q0max
  else
    q0min = q0min_reac
    q0max = q0max_reac
  end if

  kf = (3*pi**2*nneutron)**(1._dp/3)*hbarc
  tempher = 0.1_dp*(kf**2/(2*0.78*mass_neutron))
  open(21,file='testrpa_her99.d',status='unknown')


  do nq0 = 0,pts_q0
    q0 = q0min + nq0*(q0max-q0min)/pts_pol
    q0t = q0 + mu2 -mu1
    wurzel = m1*m2*(q**2 + 2*(m2-m1)*(m2-m1-q0t))
    j3_nonrel = lindhard_general(q0,q,m1,m2,mu1,mu2,temperature)
    j3_nonrel_decay = lindhard_general_decay(q0,q,m1,m2,mu1,mu2,temperature,gamdecay)
    !   lim = lindhard_imaginary(q0,q,m2,m1,mu2,mu1,temperature)
    lim = lindhard_imaginary(q0,q,m1,m2,mu1,mu2,temperature)
    lim_my = lindhard_real(q0,q,m1,m2,mu1,mu2,temperature)
    write(21,*) q0,q,q0 -( mu2 -mu1 + un - up + m1 -m2),q0t,wurzel,&
      real(j3_nonrel),aimag(j3_nonrel),&
      real(j3_nonrel_decay),aimag(j3_nonrel_decay),&
      lim,lim_my
  end do
  close(21)
 end subroutine compare_with_hernandez

 !************************************************************************

 subroutine plot_integration_over_q(omega,m1,m2,mu1,mu2,muel,munu,&
   gamdecay,q0min_reac,q0max_reac,lepton_sign,nproton,nneutron)

  use LindhardFunction, only: zero_lindhard,gamma_1,gamma_2
  use dichotomy, only : null_dicho_5
  use m_integrals

  real(dp), intent(in) :: q0min_reac,q0max_reac,m1,m2,mu1,mu2,muel,munu,omega,gamdecay,nproton,nneutron
  integer, intent(in) :: lepton_sign(3)

  character :: yes_or_no
  real(dp) :: q0min,q0max,q0testmin,q0testmax,shift,muq0,q0,gammai(2)
  real(dp) :: rpaa,rpav,iaxial,ivector,allv,alla,qmin,qmax,kf,ee,pe
  integer :: pts_q0 = 100,nq0,ifail,ngamma
  logical :: printout = .false.

  muq0 = muel - munu
  write(*,*) '***********************************************'
  write(*,*) 'Plot integration over q as function of q0'
  write(*,*) '***********************************************'
  write(*,*) 'Do you want to change minimum and maximum value for q0 with respect to chosen process (y/n)?'
  read(*,*) yes_or_no
  if(yes_or_no.eq.'y') then
    write(*,*) 'q0min?'
    read(*,*) q0min
    write(*,*) 'q0max?'
    read(*,*) q0max
  else
    q0min = q0min_reac
    q0max = q0max_reac
  end if
  open(21,file='testq0_nonrel.d', status='unknown')
  open(22,file='limitsq0_q.d', status='unknown')

  ngamma = 0
  gammai(1) = gamma_1(temperature,m1,m2,mu1,mu2,nproton,nneutron)
  gammai(2) = gamma_2(temperature,m1,m2,mu1,mu2,nproton,nneutron)
  kf = (3*pi**2*density/2)**(1._dp/3)*hbarc
  shift = -(kf**2/(2*m1) + m1-m2 + mu2-mu1)

  call null_dicho_5(zero_lindhard,q0testmin,q0testmax,m2,omega,muq0,kf,shift,ifail,printout)
  IF(ifail.eq.0) then
    q0_limit = q0testmax
    write(*,*) 'q0 limit of continuum', q0_limit,shift,omega,m1,muq0,kf
  else
    q0_limit = 0._dp
  end if

  q0loop: DO nq0 = 0,pts_q0
    q0 = q0min + nq0*(q0max-q0min)/pts_q0
    ee = lepton_sign(1)*q0 + lepton_sign(2)*omega + lepton_sign(3)*(muel - munu)
    pe = ee**2 - me**2
    if(pe < 0._dp) cycle q0loop
    pe = sqrt(pe)
    qmin = abs(pe-omega)
    qmax = pe + omega
    write(22,*) q0,qmin,qmax

    call integrate_non_relativistic_q(q0,omega,m1,m2,mu1,mu2,muq0,temperature,ivector,iaxial)
    ngamma = 1
    call integrate_non_relativistic_q_all(q0,omega,m1,m2,mu1,mu2,muel,munu,&
         temperature,lepton_sign,rpav,rpaa,ngamma,gamdecay)
    ngamma = 2
    call integrate_non_relativistic_q_all(q0,omega,m1,m2,mu1,mu2,muel,munu,&
         temperature,lepton_sign,allv,alla,ngamma,gammai(1),gammai(2))
    write(21,*) q0,ivector,iaxial,rpav,rpaa,allv,alla

  end DO q0loop


  close(21)
  close(22)
 end subroutine plot_integration_over_q

 !*************************************************************************
 !**** Compute full Durca integration for a given neutrino energy with
 !**** different approximations

 subroutine check_durca_full_integration(omega,m1,m2,mu1,mu2,muel,munu,nproton,nneutron,gamdecay)

  use LindhardFunction, only: zero_lindhard,gamma_1,gamma_2
  use dichotomy, only : null_dicho_5
  use m_integrals
  use leptons, only: determine_q0min_and_q0max_leptons

  real(dp), intent(in) :: omega,m1,m2,mu1,mu2,muel,munu,gamdecay,nproton,nneutron

  real(dp) :: inp,ipn,kf,q0min,q0max,shift,muq0
  real(dp) :: gamma1,gamma2
  integer :: ifail,which_leptons,ngamma
  logical :: printout = .false.

  write(*,*) '***********************************************'
  write(*,*) 'Full integration of Durca process with different approximations'
  write(*,*) 'for given T, n_B, Y_q and neutrino energy'
  write(*,*) '***********************************************'
  muq0 = muel - munu

  kf = (3*pi**2*density/2)**(1._dp/3)*hbarc
  shift = -(kf**2/(2*m1) + m1-m2 + mu2-mu1)
  which_leptons = 1
  call determine_q0min_and_q0max_leptons(which_leptons,temperature,omega,munu,muel,q0min,q0max)

  call null_dicho_5(zero_lindhard,q0min,q0max,m2,omega,muq0,kf,shift,ifail,printout)
  IF(ifail.eq.0) then
    q0_limit = q0max
  else
    q0_limit = 0._dp
  end if
  gamma1 = gamma_1(temperature,m1,m2,mu1,mu2,nproton,nneutron)
  gamma2 = gamma_2(temperature,m1,m2,mu1,mu2,nproton,nneutron)

  which_leptons = 1
  write(*,*) '*****************************************************'
  write(*,*) 'Electron capture reaction and its inverse for neutrino energy = ',omega, 'MeV'
  ! Durca electron capture for this conditions in mean field
  call integrate_non_relativistic_q0(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,inp,ipn)
  write(*,*) 'inp, ipn without RPA', inp,ipn
  ngamma = 1
  call integrate_non_relativistic_q0_all(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,which_leptons,inp,ipn,ngamma,gamdecay)
  write(*,*) 'With finite quasi-particle width = ', real(gamdecay)
  write(*,*) 'inp, ipn = ', inp,ipn
  ngamma = 2
  call integrate_non_relativistic_q0_all(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,which_leptons,inp,ipn,ngamma,gamma1,gamma2)
  write(*,*) 'With finite quasi-particle widths for both nucleons = ', real(gamma1),real(gamma2)
  write(*,*) 'inp,ipn = ', inp,ipn
  stop
  
  write(*,*) '*****************************************************'
  write(*,*) 'Proton decay reaction and its inverse for neutrino energy = ',omega, 'MeV'
  which_leptons = 4
  call integrate_non_relativistic_q0_eplus(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,inp,ipn)
  write(*,*) 'inp, ipn without RPA', inp,ipn
  ngamma = 1
  call integrate_non_relativistic_q0_all(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,which_leptons,inp,ipn,ngamma,gamdecay)
  write(*,*) 'With finite quasi-particle width = ', real(gamdecay)
  write(*,*) 'inp, ipn = ', inp,ipn
  ngamma = 2
  call integrate_non_relativistic_q0_all(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,which_leptons,inp,ipn,ngamma,gamma1,gamma2)
  write(*,*) 'With finite quasi-particle widths for both nucleons = ', real(gamma1),real(gamma2)
  write(*,*) 'inp,ipn = ', inp,ipn



  which_leptons = 3
  call determine_q0min_and_q0max_leptons(which_leptons,temperature,omega,muel,munu,q0min,q0max)

  call null_dicho_5(zero_lindhard,q0min,q0max,m2,omega,muq0,kf,shift,ifail,printout)
  if(ifail == 0) then
    q0_limit = q0max
    !write(*,*) 'q0 limit of continuum,nubar', q0_limit,shift,omega,m1,muq0,kf
  else
    q0_limit = 0._dp
  end if


  write(*,*) '*****************************************************'
  write(*,*) 'Neutron decay reaction and its inverse for neutrino energy = ',omega, 'MeV'

  call integrate_non_relativistic_q0_nubar(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,inp,ipn)
  write(*,*) 'inp, ipn without RPA', inp,ipn
  ngamma = 1
  call integrate_non_relativistic_q0_all(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,which_leptons,inp,ipn,ngamma,gamdecay)
  write(*,*) 'With finite quasi-particle width = ', real(gamdecay)
  write(*,*) 'inp, ipn = ', inp,ipn
  ngamma = 2
  call integrate_non_relativistic_q0_all(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,which_leptons,inp,ipn,ngamma,gamma1,gamma2)
  write(*,*) 'With finite quasi-particle widths for both nucleons = ', real(gamma1),real(gamma2)
  write(*,*) 'inp,ipn = ', inp,ipn

  write(*,*) '*****************************************************'
  write(*,*) 'Positron capture reaction and its inverse for neutrino energy = ',omega, 'MeV'
  which_leptons = 2


  call integrate_non_relativistic_q0_nubar_eplus(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,inp,ipn)
  write(*,*) 'inp, ipn without RPA', inp,ipn
  ngamma = 1
  call integrate_non_relativistic_q0_all(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,which_leptons,inp,ipn,ngamma,gamdecay)
  write(*,*) 'With finite quasi-particle width = ', real(gamdecay)
  write(*,*) 'inp, ipn = ', inp,ipn
  ngamma = 2
  call integrate_non_relativistic_q0_all(omega,m1,m2,mu1,mu2,muel,&
    munu,temperature,which_leptons,inp,ipn,ngamma,gamma1,gamma2)
  write(*,*) 'With finite quasi-particle widths for both nucleons = ', real(gamma1),real(gamma2)
  write(*,*) 'inp,ipn = ', inp,ipn


 end subroutine check_durca_full_integration




end module main_durca_m
