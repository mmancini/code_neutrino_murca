module murca_debug_m
 use precision

 implicit none


#include "common_neutrino.h"

  private



  interface debug_write_verbose
    module procedure debug_write_verbose_2,debug_write_verbose_4
    module procedure debug_write_verbose_array, debug_write_verbose_array_only
    module procedure debug_write_verbose_string
  end interface debug_write_verbose


  integer,public,parameter :: &
    &                  file_murca_mc_3a_12 = 21,&
    &                  file_murca_mc_3a_34 = 22,&
    &                  file_murca_mc_3c_12 = 23,&
    &                  file_murca_mc_3c_34 = 24,&
    &                  file_murca_mc_3d_12 = 25,&
    &                  file_murca_mc_3d_34 = 26,&
    &                  file_murca_durca = 27,   &
    &                  file_murca_durca_nb = 28

  public :: debug_write_verbose


 contains

  subroutine debug_write_verbose_2(unit,jj,dens)
   integer,intent(in) :: unit
   integer(i64),intent(in) :: jj
   real(dp),intent(in) :: dens(2)
#ifdef HAVE_DEBUG_VERBOSE_1TH
   write(unit,*) jj,dens(1)/real(jj,dp),dens(2)/real(jj,dp)
#endif
  end subroutine debug_write_verbose_2


  subroutine debug_write_verbose_4(unit,jj,dens)
   integer,intent(in) :: unit
   integer(i64),intent(in) :: jj
   real(dp),intent(in) :: dens(4,4)

#ifdef HAVE_DEBUG_VERBOSE_1TH
   write(unit,*) jj,dens(:,1)/real(jj,dp),dens(:,2)/real(jj,dp),dens(:,3)/real(jj,dp),dens(:,4)/real(jj,dp)
#endif
  end subroutine debug_write_verbose_4


  subroutine debug_write_verbose_array(unit,jj,dens,array)
   integer,intent(in) :: unit
   integer(i64),intent(in) :: jj
   real(dp),intent(in) :: dens(4,4)
   real(dp),intent(in) :: array(:)

#ifdef HAVE_DEBUG_VERBOSE_1TH
   write(unit,*) jj,dens(:,1)/real(jj,dp),dens(:,2)/real(jj,dp),dens(:,3)/real(jj,dp),&
     &              dens(:,4)/real(jj,dp), array(:)
#endif
  end subroutine debug_write_verbose_array

  subroutine debug_write_verbose_array_only(unit,array)
   integer,intent(in) :: unit
   real(dp),intent(in) :: array(:)

#ifdef HAVE_DEBUG_VERBOSE_1TH
   write(unit,*)  array(:)
#endif
  end subroutine debug_write_verbose_array_only


  subroutine debug_write_verbose_string(unit,string)
   integer,intent(in) :: unit
   character(*),intent(in) :: string

#ifdef HAVE_DEBUG_VERBOSE_1TH
   write(unit,*)  string
#endif
  end subroutine debug_write_verbose_string



end module murca_debug_m
