!! The APR potential model from Schneider, Constantinou et al.
module apr_eos

  use precision
  use integration_densities, only: fermi_12,fermi_m12,fermi_32,fermi_12_inv
  use eos_compose, only: interpolate_symmetry_energy
  implicit none

  private

  real(dp):: p_i(21),ntr_coeff(5)

  public ::  &
    &        set_apr_parameters

 contains

    subroutine set_apr_parameters

    namelist/aprlist/ p_i,ntr_coeff

    integer :: io,readeof

    open(15,file='para_eos.nl', status='old',iostat=io)
    if(io.ne.0) then
       write(*,*) 'No parameter file for EoS related parameters found'
       write(*,*) 'Create one with APR parameters entered'
       open(15,file='para_eos.nl', status='new')
    end if
    rewind(15)
    read(15,nml=aprlist,iostat=readeof)
    if(readeof.ne.0) then
       write(*,*) 'Parameter list for APR interaction not found in para_eos.nl'
       write(*,*) 'Create new one'
       p_i(:) = 0._dp
       p_i(1) = 337.2
       p_i(2) = -382.0
       p_i(3) = 89.8
       p_i(4) = 0.457
       p_i(5) = -59.0
       p_i(6) = -19.1
       p_i(7) = 214.6
       p_i(8) = - 384.0
       p_i(9) = 6.4
       p_i(10) = 69.0
       p_i(11) = -33.0
       p_i(12) = 0.35
       p_i(15) = 287.0
       p_i(16) = -1.54
       p_i(17) = 175.0
       p_i(18) = -1.45
       p_i(19) = 0.32
       p_i(20) = 0.195
       ntr_coeff(1) = 0.1956
       ntr_coeff(2) = 0.3389
       ntr_coeff(3) = 0.2918
       ntr_coeff(4) = -1.2614
       ntr_coeff(5) = 0.6307
       write(15,nml=aprlist)

    end if
    close(15)
  end subroutine set_apr_parameters



end module apr_eos
