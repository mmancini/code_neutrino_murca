#ifndef COMMON_NEUTRINO_H
#define COMMON_NEUTRINO_H

# ifdef HAVE_DEBUG_VERBOSE_1TH
#  define DBG_VERB_OPEN(a,b) open(a,file=b,status='unknown')
#  define DBG_VERB_CLOSE(a) close(a)
# else
#  define DBG_VERB_OPEN(a,b)
#  define DBG_VERB_CLOSE(a)
# endif

#endif
