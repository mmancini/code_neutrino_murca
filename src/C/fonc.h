#ifndef __FONC_H_
#define __FONC_H_
#include "matrice.h"

class Fonction {

    // Donnees :
    // -------
protected:
    int npoints ;
    Tab xx ;
    Tab ff ;
    Tab coef ;
    bool coef_connus ;
    Matrice trans ;

protected:
    void initialise_grille() ;

    // Constructeurs - Destructeur
    // ---------------------------
public:
    explicit Fonction(int ) ; ///< constructeur standard, initialise a zero
    explicit Fonction(const Tab&) ; ///< constructeur depuis le tableau des valeurs
    Fonction(int, const char* ) ; ///< construteur depuis un fichier
    Fonction(const Fonction&) ; ///< recopie

    virtual ~Fonction() ;

    // Affectation
    // -----------
    void operator=(const Fonction&) ;
    void operator=(const Tab&) ;
    void operator=(double) ;

    void set_coef(const Tab&) ;

    // Calculs
    //--------
    void calcule_coef() ;
    double calcule_en_x(double) const ;
    void inverse_coef() ;
    Fonction derivee() const ;

    // Sauvegarde dans un fichier
    // --------------------------
    void sauve(const char* nom_fich) const ;

    // Acces aux donnees
    //------------------
    const Tab& grille() const { return xx ; } ;
    double get_coef(int i) const {
      assert (coef_connus) ;
      return coef(i);
    } ;

    friend std::ostream& operator<<(std::ostream&, const Fonction& ) ; //la fonction externe
                                                             //a appeler
    friend Fonction operator-(const Fonction&) ;
    friend Fonction operator+(const Fonction&, const Fonction&) ;
    friend Fonction operator+(const Fonction&, double) ;
    friend Fonction operator+(double, const Fonction&) ;
    friend Fonction operator-(const Fonction&, const Fonction&) ;
    friend Fonction operator-(const Fonction&, double) ;
    friend Fonction operator-(double, const Fonction&) ;
    friend Fonction operator*(const Fonction&, const Fonction&) ;
    friend Fonction operator*(const Fonction&, double) ;
    friend Fonction operator*(double, const Fonction&) ;
    friend Fonction operator/(const Fonction&, const Fonction&) ;
    friend Fonction operator/(const Fonction&, double) ;
    friend Fonction operator/(double, const Fonction&) ;

    friend Fonction sin(const Fonction&) ;
    friend Fonction exp(const Fonction&) ;
    friend Fonction sqrt(const Fonction&) ;
    friend Fonction pow(const Fonction&, double) ;
    friend Fonction abs(const Fonction&) ;
    friend double max(const Fonction&) ;
};

std::ostream& operator<<(std::ostream&, const Fonction& ) ;
Fonction operator-(const Fonction&) ;
Fonction operator+(const Fonction&, const Fonction&) ;
Fonction operator+(const Fonction&, double) ;
Fonction operator+(double, const Fonction&) ;
Fonction operator-(const Fonction&, const Fonction&) ;
Fonction operator-(const Fonction&, double) ;
Fonction operator-(double, const Fonction&) ;
Fonction operator*(const Fonction&, const Fonction&) ;
Fonction operator*(const Fonction&, double) ;
Fonction operator*(double, const Fonction&) ;
Fonction operator/(const Fonction&, const Fonction&) ;
Fonction operator/(const Fonction&, double) ;
Fonction operator/(double, const Fonction&) ;

Fonction sin(const Fonction&) ;
Fonction exp(const Fonction&) ;
Fonction sqrt(const Fonction&) ;
Fonction pow(const Fonction&, double) ;
Fonction abs(const Fonction&) ;
double max(const Fonction&) ;

#endif
