#ifndef __MATRICE_H_
#define __MATRICE_H_
#include "tab.h"

class Matrice: public Tab {

    // Constructeurs - Destructeur
    // ---------------------------
 public:
  explicit Matrice(int ) ;
  Matrice (const Tab&) ; //conversion depuis un Tab
  Matrice (const Matrice& ) ; //constructeur par recopie
  explicit Matrice(const char* ) ; // constructeur depuis un fichier

  virtual ~Matrice() ;

    // Affectation
    // -----------
  void operator=(const Matrice&) ; //affectation depuis une autre Matrice
  void operator=(const Tab&) ; //affectation depuis un autre Tab
  void operator=(double) ; //affectation depuis un double

  // Multiplication membre
  //----------------------
  virtual Tab operator*(const Tab&) const ;

  // Fonctions de calcul (virtuelles, pour d'eventuelles classes derivees)
  // ---------------------------------------------------------------------
  virtual Tab resout(const Tab& rhs) const ; //resout le systeme A.x = y
                           // avec A -> this, y -> rhs et x valeur de retour

  virtual Matrice inverse() const ;

  virtual double determinant() const ;

 protected:
  virtual void affiche(std::ostream& ) const ; //Affichage


};

// Declaration pour les appels a LAPACK
extern "C" {
    void dgetrf_(int*, int*, double[], int*, int[], int *) ;
    void dgetrs_(const char*, int*, int*, double[], int*, int[], double [], int*, int* ) ;
    void dgetri_(int*, double[], int*, int[], double[], int*, int*) ;
    void dgeev_(const char*, const char*, int*, double[], int*, double[], double[],
	       double[], int*, double[], int*, double[], int*, int*) ;
}
#endif
