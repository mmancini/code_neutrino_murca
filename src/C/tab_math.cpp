#include <cmath>
#include "tab.h"

//#include"arithmetic.hpp"

// operations mathematiques
Tab apply(const Tab& t, double (*p_fonc)(double)) {
  Tab result(t);
  int taille=result.get_nelt();
  for(int i=0; i<taille; i++)
    result.tableau[i] = (*p_fonc)(result.tableau[i]);
  return result;
}

Tab sin(const Tab& t) {return apply(t,sin);}
Tab cos(const Tab& t) {return apply(t,cos);}
Tab tan(const Tab& t) {return apply(t,tan);}
Tab exp(const Tab& t) {return apply(t,exp);}
Tab log(const Tab& t) {return apply(t,log);}
Tab sqrt(const Tab& t) {return apply(t,sqrt);}
Tab abs(const Tab& t) {return apply(t,fabs);}

// operateurs binaires
Tab apply(const Tab& t1, const Tab& t2, double (*p_fonc)(double,double)) {
  assert(t1.check_dims(t2));
  Tab result(t1);
  int taille=result.get_nelt();
  for(int i=0; i<taille; i++)
    result.tableau[i] = (*p_fonc)(t1.tableau[i],t2.tableau[i]);
  return result;
}
Tab apply(const Tab& t, double r, double (*p_fonc)(double,double)) {
  Tab result(t);
  int taille=result.get_nelt();
  for(int i=0; i<taille; i++)
    result.tableau[i] = (*p_fonc)(t.tableau[i],r);
  return result;
}
Tab apply(double r, const Tab& t, double (*p_fonc)(double,double)) {
  Tab result(t);
  int taille=result.get_nelt();
  for(int i=0; i<taille; i++)
    result.tableau[i] = (*p_fonc)(r,t.tableau[i]);
  return result;
}

// operateurs arithmetiques:
Tab operator+(const Tab& t1, const Tab& t2) {return apply(t1,t2,add);}
Tab operator+(const Tab& t, double r) {return apply(t,r,add);}
Tab operator+(double r, const Tab& t) {return apply(r,t,add);}
Tab operator-(const Tab& t1, const Tab& t2) {return apply(t1,t2,subtract);}
Tab operator-(const Tab& t, double r) {return apply(t,r,subtract);}
Tab operator-(double r, const Tab& t) {return apply(r,t,subtract);}
Tab operator/(const Tab& t1, const Tab& t2) {return apply(t1,t2,divide);}
Tab operator/(const Tab& t, double r) {return apply(t,r,divide);}
Tab operator/(double r, const Tab& t) {return apply(r,t,divide);}
Tab operator*(const Tab& t, double r) {return apply(t,r,multiply);}
Tab operator*(double r, const Tab& t) {return apply(r,t,multiply);}

Tab pow(const Tab& t, double r) {return apply(t, r, pow);}

double max(const Tab& tin) {
  int taille = tin.get_nelt() ;
  double resu = tin.tableau[0] ;
  for (int i=1; i<taille; i++)
      resu = (resu > tin.tableau[i] ? resu : tin.tableau[i] ) ;
  return resu ;
}

Tab operator-(const Tab& tin) {
  int s1 = tin.get_taille1() ;
  int s2 = tin.get_taille2() ;
  int s3 = tin.get_taille3() ;
  int taille = s1*s2*s3 ;

  Tab resu(s1, s2, s3) ;
  for (int i=0; i<taille; i++)
      resu.tableau[i]  = -tin.tableau[i] ;
  return resu ;
}
