#ifndef __TABSPEC_H_
#define __TABSPEC_H_

#include<string>
#include<iostream>
#include"arithmetic.hpp"


class TabSpec {

    // Donnees :
    // -------
 protected:
  int sizex ;
  int sizey ;
  int sizez ;
  double* tableau ;

    // Constructeurs - Destructeur
    // ---------------------------
 public:
  explicit TabSpec(int dim1, int dim2=1, int dim3=1) ; //constructeur standard
  explicit TabSpec(const std::string& ) ; // constructeur depuis un fichier
  TabSpec(const TabSpec&) ; //constructeur par recopie

  virtual ~TabSpec() ; //destructeur

    // Affectation
    // -----------
  void operator=(const TabSpec&) ; //affectation depuis un autre TabSpec
  void operator=(double) ; //affectation depuis un double


    // Acces aux donnees
    // -----------------
  int get_sizex() const {return sizex ; };
  int get_sizey() const {return sizey ; };
  int get_sizez() const {return sizez ; };
  int get_nelt() const {return sizex*sizey*sizez ; } ;
  int get_dim() const ;

  double operator()(int i, int j=0, int k=0) const ;

  double& set(int i, int j=0, int k=0) ;

    // Sauvegarde dans un fichier
    // --------------------------
  void write_file(const std::string&) const ;

 protected:
  // Verif des tailles
  bool check_sizes(const TabSpec&) const ;
  void resize(int, int=1, int=1) ;

  // Affichage
  virtual void display(std::ostream& ) const ; //pour utiliser le polymorphisme

 public:  //"public" inutile ici...
  friend std::ostream& operator<<(std::ostream&, const TabSpec& ) ; //la fonction externe
                                                      //a appeler
  // operations mathematiques:
  friend TabSpec apply(const TabSpec& t, double (*p_fonc)(double));
  friend TabSpec apply(const TabSpec& t1, const TabSpec& t2, double (*p_fonc)(double,double));
  friend TabSpec apply(const TabSpec& t, double r, double (*p_fonc)(double,double));
  friend TabSpec apply(double r, const TabSpec& t, double (*p_fonc)(double,double));
  friend double max(const TabSpec& t);
  friend TabSpec operator-(const TabSpec&) ;

};

TabSpec operator+(const TabSpec&, const TabSpec&) ;
TabSpec operator+(const TabSpec&, double) ;
TabSpec operator+(double, const TabSpec&) ;
TabSpec operator-(const TabSpec&, const TabSpec&) ;
TabSpec operator-(const TabSpec&, double) ;
TabSpec operator-(double, const TabSpec&) ;
TabSpec operator-(const TabSpec&) ;
TabSpec operator*(const TabSpec&, const TabSpec&) ;
TabSpec operator*(const TabSpec&, double) ;
TabSpec operator*(double, const TabSpec&) ;

#endif
