#include<iomanip>
#include "matrice.h"

using namespace std ;

Matrice::Matrice(int dim1) : Tab(dim1, dim1) { }

Matrice::Matrice(const char* nom_fich) : Tab(nom_fich){
  assert((taille1 == taille2)&&(taille3 == 1)) ;  }

Matrice::Matrice(const Tab& tab_in) : Tab(tab_in) {
    assert(tab_in.get_taille1() == tab_in.get_taille2()) ;
    assert(tab_in.get_taille3() == 1) ;  }

Matrice::Matrice(const Matrice& tab_in) : Tab(tab_in) {}

Matrice::~Matrice() {}

void Matrice::operator=(const Matrice& tab_in) {
  Tab::operator=(tab_in) ;  }

void Matrice::operator=(const Tab& tab_in) {
  assert(tab_in.get_taille1() == tab_in.get_taille2()) ;
  assert(tab_in.get_taille3() == 1) ;
  assert(taille1 = tab_in.get_taille1()) ;

  Tab::operator=(tab_in) ;  }

void Matrice::operator=(double x) { Tab::operator=(x) ; }

Tab Matrice::operator*(const Tab& tin) const {
    int s1 = tin.get_taille1() ;
    int s2 = tin.get_taille2() ;
    int s3 = tin.get_taille3() ;
    assert((taille2 == s1)&&(s3 == 1)) ;
    assert((s1==s2)||(s2==1)) ;

    Tab resu(s1, s2) ;
    for (int i=0; i<s1; i++)
	for (int j=0; j<s2; j++) {
	    double somme = 0 ;
	    for (int k=0; k<s1; k++)
		somme += (*this)(i, k)*tin(k,j) ;
	    resu.set(i,j) = somme ;
	}
    return resu ; }

void Matrice::affiche(std::ostream& ost) const {
  assert(tableau != 0x0) ;

  ost << "Matrice carree " << taille1 << "x" << taille2 << endl ;
  ost << setprecision(5) ;
  for (int i=0; i<taille1; i++) {
    for (int j=0; j<taille2; j++) {
      ost << tableau[i*taille2 + j] << '\t' ;
    }
    ost << std::endl ;
  }
  ost << endl ;  }
