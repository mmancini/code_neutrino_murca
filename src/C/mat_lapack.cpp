#include<cstdlib>
#include<iomanip>
#include "matrice.h"

Tab Matrice::resout(const Tab& source) const {
    int n= taille1 ;
    Matrice mat_lu(*this) ;
    int* permute = new int[n] ;
    int ldab, info ;
    ldab = n ;
    dgetrf_(&n, &n, mat_lu.tableau, &ldab, permute, &info) ;

    const char* trans = "T" ;
    int nrhs = 1 ;
    int ldb = n ;
    assert (source.get_taille1() == n) ;
    assert ( (source.get_taille2() == 1) && ( source.get_taille3() == 1)) ;
    Tab res(source) ;
    dgetrs_(trans, &n, &nrhs, mat_lu.tableau, &ldab, permute, res.tableau, &ldb, &info) ;

    delete [] permute ;
    return res ;
}

Matrice Matrice::inverse() const {
    int n=taille1 ;
    Matrice resu(*this) ;
    int* permute = new int[n] ;
    int ldab, info ;
    ldab = n ;
    dgetrf_(&n, &n, resu.tableau, &ldab, permute, &info) ;

    int lda = n ;
    int lwork = n ;
    double* work = new double[lwork] ;
    dgetri_(&n, resu.tableau, &lda, permute, work, &lwork, &info) ;
    delete [] permute ;
    delete [] work ;

    return resu ;

}

double Matrice::determinant() const {

    const char* jobvl = "N" ;
    const char* jobvr = "N" ;
    int n = taille1 ;
    double* a = new double [n*n] ;
    for (int i=0 ; i<n*n ; i++)
	a[i] = tableau[i] ;
    int lda = n ;
    double* wr = new double[n] ;
    double* wi = new double[n] ;
    int ldvl = 1 ;
    double* vl = 0x0 ;
    int ldvr = 1 ;
    double* vr = 0x0 ;
    int ldwork = 3*n ;
    double* work = new double[ldwork] ;
    int info ;
    dgeev_(jobvl, jobvr, &n, a, &lda, wr, wi, vl, &ldvl, vr, &ldvr,
	    work, &ldwork, &info) ;
    
    double resu = 1 ;
    for (int i=0 ; i<n ; i++) {
	if (wi[n-i-1] == 0)
	    resu *= wr[n-i-1] ;
	else {
	    resu *= wr[n-i-1]*wr[n-i-1] +  wi[n-i-1]* wi[n-i-1] ;
	    i++;
	}
    }
    delete [] wr ;
    delete [] wi ;
    delete [] a ;
    delete [] work ;
    return resu ;
}

