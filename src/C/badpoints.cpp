// Lorene headers
#include<fstream>
#include<cmath>
#include<cstdlib>
#include<iomanip>
#include<cassert>
#include"tabspec.hpp"

using namespace std ;


double search_jump(const TabSpec& coefs, const TabSpec& rr, const TabSpec& limits,
		   int ipt) ;

double compute_in_x(const TabSpec& coefs, double enu,
		     const TabSpec& limits) {
  int nzone = limits.get_sizex() ;
  assert ( (enu >= limits(0,0)) && (enu <= limits(nzone-1,1)) ) ;
  int indz = -1 ;
  for (int i=0; i<nzone; i++) {
    if ( (limits(i,0) <=enu ) && (enu <= limits(i,1)) ) {
      indz = i ;
      break;
    }
  }

  double alpha = 0.5*(limits(indz, 1) - limits(indz, 0)) ;
  double beta = 0.5*(limits(indz, 1) + limits(indz, 0)) ;
  double x = (enu - beta)/alpha ;
  int npt = coefs.get_sizey() ;
  double resu = coefs(indz, npt-1);

  for (int i=npt-2; i>=0; i--) {
    resu = resu*x + coefs(indz, i) ;
  }
  return resu ;
}

double compute_der_x(const TabSpec& coefs, double enu,
		     const TabSpec& limits) {
  int nzone = limits.get_sizex() ;
  assert ( (enu >= limits(0,0)) && (enu <= limits(nzone-1,1)) ) ;
  int indz = -1 ;
  for (int i=0; i<nzone; i++) {
    if ( (limits(i,0) <=enu ) && (enu <= limits(i,1)) ) {
      indz = i ;
      break;
    }
  }

  double alpha = 0.5*(limits(indz, 1) - limits(indz, 0)) ;
  double beta = 0.5*(limits(indz, 1) + limits(indz, 0)) ;
  double x = (enu - beta)/alpha ;
  int npt = coefs.get_sizey() ;
  double resu = (npt-1)*coefs(indz, npt-1);

  for (int i=npt-2; i>0; i--) {
    resu = resu*x + i*coefs(indz, i) ;
  }
  return resu ;
}

int main(int argc, char** argv) {

  if (argc != 3) {
    cerr << "Error in call to bad_points: needs two arguments." << endl ;
    cerr << "Usage : badpoints input_file output_file" << endl ;
    abort() ;
  }
  ifstream fich(argv[1]) ;

  ofstream outfile(argv[2], ofstream::app) ;

  double threshold = 0.6 ;
  int count_max = 3 ; // 3 should be a minimum
  if (count_max < 3) {
    cerr << "badpoints: count_max must be larger than 3!" << endl ;
    abort() ;
  }
  // neutrinos' max mean free path to trigger the "bad point" test
  double nu_mfp_max = 1.e12 ; // in cm
  double level = log(1./nu_mfp_max) ;
  int ipt = -1 ; int ijump = -1 ; double jump_thres = log(1.e3) ; double ejump= -1000.;

  int indx, indy, indz ;
  int nzone ;
  int npt = 9 ;

  fich >> indx >> indy >> indz ;

  fich >> nzone ;
  TabSpec coefs(nzone, npt) ;
  TabSpec limits(nzone, 2) ;
  for (int i=0; i<nzone; i++)
    fich >> limits.set(i, 0) ; // Emin
  for (int i=0; i<nzone; i++)
    fich >> limits.set(i, 1) ; // Emax
  for (int i=0; i<nzone; i++) {
    for (int j=0; j<npt; j++) {
      fich >> coefs.set(i, j) ;
    }
  }

  TabSpec xx(npt) ;
  for (int i=0; i<npt; i++)
    xx.set(i) = -cos(double(i)*M_PI/double(npt-1)) ;

  int izone_bad[20];
  int nbad;
  int njump;
  njump = 0;
  nbad = 0;
  double amplmax = -1. ;
  for (int izone=0; izone<nzone; izone++) {
    int counter = 0 ;
    double ampl = 0. ;
    double eps = 1.e-15 ;
    double alpha = 0.5*(limits(izone, 1) - limits(izone, 0))*(1.-eps) ;
    double beta = 0.5*(limits(izone, 1) + limits(izone, 0)) ;
    TabSpec rr = alpha*xx + beta ;
    TabSpec valfunc(npt) ;
    TabSpec derval(npt) ;
    TabSpec indices(npt) ;
    indices = 0 ;

    for (int i=0; i<npt; i++) {
      derval.set(i) = compute_der_x(coefs, rr(i), limits) ;
      valfunc.set(i) = compute_in_x(coefs, rr(i), limits) ;
      if (i>0) {
	double ampl_val = fabs(valfunc(i) - valfunc(i-1)) ;
	if (ampl_val > jump_thres) {
	  ijump = izone ;
	  ipt = i ;
	}

	if (derval(i-1)*derval(i) < 0) {
	  if (valfunc(i) > level) {
	    indices.set(counter) = i ;
	    counter++ ;
	  }
	}
      }
    }

    if (counter > count_max) {
      for (int i=1; i<counter-1; i++) {
	ampl += fabs(derval(indices(i)) - derval(indices(i)-1)) ;
      }
      ampl /= double(counter-2) ;
    }

    if ( (counter > count_max) && (ampl > threshold) ) {
      izone_bad[nbad] = izone+1;
      nbad = nbad + 1;
      if(ampl > amplmax) amplmax = ampl;
      cout << indx << '\t' << indy << '\t' << indz << '\t' << amplmax <<'\t' << nbad << '\t'<< izone+1<<endl ;
      if (ijump != -1) {
	ejump = search_jump(coefs, rr, limits, ipt ) ;
	cout << '\t' << ejump << '\t' << ijump+1 <<endl;
	njump = ijump+1;
	ijump = -1 ;
      }

    }
  }
  if(amplmax > 0.) {
    outfile << indx << '\t' << indy << '\t' << indz << '\t' << amplmax
	    << '\t' << nzone << '\t' << nbad;
    for(int i = 0; i < nbad; i++)
      outfile << '\t' << izone_bad[i] ;
    for (int i=0; i<nzone; i++)
      outfile << setprecision(16) << '\t' << limits(i,0) << '\t' << limits(i,1) ;
    outfile << '\t' << ejump << '\t' << njump <<endl;
  }


  return EXIT_SUCCESS ;
}

double search_jump(const TabSpec& coefs, const TabSpec& rr, const TabSpec& limits,
		   int ipt ) {

  double threshold = 1.e-14 ;

  double xp = rr(ipt) ;
  double xm = rr(ipt-1) ;

  double fp = compute_in_x(coefs, xp, limits) ;
  double fm = compute_in_x(coefs, xm, limits) ;

  double emean = 0.5*(fp + fm) ;
  fp -= emean ;
  fm -= emean ;
  int iter = 0 ;
  int itermax = 100 ;

  while ((fabs(xp - xm)/(fabs(xp) + fabs(xm)) > threshold) && iter < itermax) {
    double xnew = (fp*xm - fm*xp)/(fp - fm) ;
    double fnew = compute_in_x(coefs, xnew, limits) - emean ;
    if (fnew*fp > 0) {
      fp = fnew ;
      xp = xnew ;
    }
    else {
      fm = fnew ;
      xm = xnew ;
    }
    iter++ ;
  }
  return 0.5*(xp + xm) ;

}
