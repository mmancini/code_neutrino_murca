#include<fstream>
#include<iomanip>
#include "tab.h"

using namespace std ;


Tab::Tab(int dim1, int dim2, int dim3) : taille1(dim1), taille2(dim2), taille3(dim3),
					 tableau(0x0) {
  assert((dim1>0)&&(dim2>0)&&(dim3>0)) ;
  tableau = new double[dim1*dim2*dim3] ;
}

Tab::Tab(const char* nom_fich) {
  ifstream fich(nom_fich) ;
  assert(fich) ;
  fich >> taille1 >> taille2 >> taille3 ;
  assert((taille1>0)&&(taille2>0)&&(taille3>0)) ;
  int t_tot = taille1*taille2*taille3 ;
  tableau = new double[t_tot] ;
  for (int i=0; i<t_tot; i++) {
    assert(fich) ;
    fich >> tableau[i] ;
  }
}

Tab::Tab(const Tab& tab_in) : taille1(tab_in.taille1), taille2(tab_in.taille2),
			      taille3(tab_in.taille3), tableau(0x0) {

    assert((taille1>0)&&(taille2>0)&&(taille3>0)) ;
    int t_tot = taille1*taille2*taille3 ;
    tableau = new double[t_tot] ;
    for (int i=0; i<t_tot; i++)
      tableau[i] = tab_in.tableau[i] ;
}

Tab::~Tab() {
  delete [] tableau ;
}

bool Tab::check_dims(const Tab& t) const {
  bool resu = true ;
  resu = resu && (taille1 == t.taille1) ;
  resu = resu && (taille2 == t.taille2) ;
  resu = resu && (taille3 == t.taille3) ;

  return resu;
}

void Tab::operator=(const Tab& tab_in) {
  assert(check_dims(tab_in)) ;

  int t_tot = taille1*taille2*taille3 ;
  for (int i=0; i<t_tot; i++)
    tableau[i] = tab_in.tableau[i] ;
}

void Tab::operator=(double x) {

  int t_tot = taille1*taille2*taille3 ;
  for (int i=0; i<t_tot; i++)
    tableau[i] = x ;
}

void Tab::sauve(const char* nom_fich) const {

  ofstream fich(nom_fich) ;
  int t_tot = taille1*taille2*taille3 ;
  fich << taille1 << '\t' << taille2 << '\t' << taille3 << endl ;
  fich << setprecision(16) ;
  for (int i=0; i<t_tot; i++)
    fich << tableau[i] << '\t' ;
  fich << endl ;
}

Tab Tab::operator*(const Tab& tin) const
{
  return apply((*this), tin, multiply) ;
}

void Tab::affiche(ostream& ost) const {

  int dimens = 0 ;
  if (taille1 > 1) dimens++ ;
  if (taille2 > 1) dimens++ ;
  if (taille3 > 1) dimens++ ;
  ost << "Tab a " << dimens << " dimension" << (dimens>1 ? "s:\n" : ":\n") ;
  if (taille1 > 1) {
      ost << taille1 ;
      if (dimens>1) {
	  ost << "x" ;
	  dimens-- ;
      }
  }
  if (taille2>1) {
      ost << taille2 ;
      if (dimens>1) ost << "x" ;
  }
  if (taille3>1) ost << taille3 ;
  ost << " elements" << endl ;
  ost << setprecision(5) ;

  if (taille3 == 1) {
      for (int i=0; i<taille1; i++) {
	  for (int j=0; j<taille2; j++) {
	      ost << tableau[i*taille2 + j] << '\t' ;
	  }
	  if (taille2 >1) ost << endl ;
      }
      ost << endl ;
  }
  else {
      for (int i=0; i<taille1; i++) {
	  ost << "i=" << i << '\n' ;
	  for (int j=0; j<taille2; j++) {
	      for (int k=0; k<taille3; k++) {
		  ost << tableau[(i*taille2+j)*taille3 + k] << '\t' ;
	      }
	      ost << endl ;
	  }
	  ost << endl ;
      }
      ost << endl ;
  }
  return ;
}
ostream& operator<<(ostream& ost, const Tab& tab_in ) {
  tab_in.affiche(ost) ;
  return ost ;
}
