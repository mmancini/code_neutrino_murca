#include<cstdlib>
#include<fstream>
#include<iomanip>
#include<cmath>
#include"fonc.h"

using namespace std ;

void Fonction::initialise_grille() {

    int nm1 = npoints-1 ;
    // Points de grille
    for (int i=0; i<npoints; i++)
	xx.set(i) = -cos(double(i*M_PI)/double(nm1)) ;

    // Poids
    Tab ww(npoints) ;
    ww = M_PI / double(nm1) ;
    ww.set(0) *= 0.5 ;
    ww.set(nm1) *= 0.5 ;

    // Matrice de transformation
    for (int l=0; l<npoints; l++) {
	for (int c=0; c<npoints; c++)
	    trans.set(l, c) = cos(double(l*(nm1-c))*M_PI/double(nm1)) ;
	double gamma_l = 0 ;
	for (int i=0; i<npoints; i++)
	    gamma_l += trans(l, i)*trans(l, i)*ww(i) ;
	for (int c=0; c<npoints; c++)
	    trans.set(l, c) *= ww(c)/gamma_l ;
    }
}

Fonction::Fonction(int n): npoints(n), xx(n), ff(n), coef(n), coef_connus(false),
			   trans(n)
{
    assert(n>0) ;
    initialise_grille() ;
    ff = 0 ;
    coef = 0 ;
    coef_connus = true ;
}

Fonction::Fonction(const Tab& tbl) : npoints(tbl.get_taille1()), xx(tbl.get_taille1()),
				     ff(tbl), coef(tbl.get_taille1()),
				     coef_connus(false), trans(tbl.get_taille1())
{
    assert(tbl.get_taille2() == 1) ;
    assert(tbl.get_taille3() == 1) ;
    initialise_grille() ;
}

Fonction::Fonction(int n, const char* fichier) : npoints(n), xx(n), ff(n),
						 coef(n), coef_connus(false), trans(n)
{
  initialise_grille() ;
  ifstream fich(fichier) ;
  assert(fich) ;
  char tmp_c ;
  int tmp_n ;
  double tmp_x ;
  fich >> tmp_c >> tmp_n ;
  assert(tmp_n == n) ;
  for (int i=0; i<npoints; i++) {
    assert(fich) ;
    fich >> tmp_x >> ff.set(i) ;
  }
}

Fonction::Fonction(const Fonction& g) : npoints(g.npoints), xx(g.xx), ff(g.ff),
					coef(g.coef), coef_connus(g.coef_connus),
					trans(g.trans)
{ }

Fonction::~Fonction() { }

void Fonction::operator=(const Fonction& g) {
    assert(npoints == g.npoints) ;
    ff = g.ff ;
    coef = g.coef ;
    coef_connus = g.coef_connus ;
    return ;
}

void Fonction::operator=(const Tab& tbl)
{
    assert(npoints == tbl.get_taille1()) ;
    assert(tbl.get_taille2() == 1) ;
    assert(tbl.get_taille3() == 1) ;
    ff = tbl ;
    coef_connus = false ;
    return ;
}

void Fonction::operator=(double x)
{
    ff = x ;
    coef_connus = false ;
    return ;
}

void Fonction::set_coef(const Tab& tbl)
{
    assert(npoints == tbl.get_taille1()) ;
    assert(tbl.get_taille2() == 1) ;
    assert(tbl.get_taille3() == 1) ;
    coef = tbl ;
    coef_connus = true ;
    inverse_coef() ;
    return ;
}

void Fonction::calcule_coef()
{
    if (!coef_connus) {
	coef = trans*ff ;
	coef_connus = true ;
    }
    return ;
}

double Fonction::calcule_en_x(double x) const {
    assert(coef_connus) ;
    double resu = 0. ;
    double ci = 1. ;
    double cip1 = x ;
    for (int i=0; i<npoints; i++) {
	resu += coef(i)*ci ;
	double cip2 = 2*x*cip1 - ci ;
	ci = cip1 ;
	cip1 = cip2 ;
    }
    return resu ;
}

void Fonction::inverse_coef() {
    assert(coef_connus) ;
    for (int i=0; i<npoints; i++) {
	ff.set(i) = calcule_en_x(xx(i)) ;
    }
    return ;
}

Fonction Fonction::derivee() const {
    Fonction resu(npoints) ;
    Tab der(npoints) ;
    der = 0 ;
    for (int i=0; i<npoints; i++)
	for (int p=i+1; p<npoints; p++)
	    if ((p+i)%2 == 1)
		der.set(i) += 2*p*coef(p) ;
    der.set(0) *= 0.5 ;
    resu.set_coef(der) ;
    return resu ;
}

void Fonction::sauve(const char* nom_fich) const {

    assert(nom_fich != 0x0) ;
    ofstream sortie(nom_fich) ;
    sortie << '#' << '\t' << npoints << endl ;
    sortie << setprecision(16) ;
    for (int i=0; i<npoints; i++)
	sortie << xx(i) << '\t' << ff(i) << endl ;
    return ;
}

ostream& operator<<(ostream& o, const Fonction& f) {

    o << "Fonction representee sur " << f.npoints << " points." << endl ;
    o << "Valeurs aux points de grille: " << endl ;
    o << f.ff ;
    if (!f.coef_connus)
	o << "Coefficients non connus." << endl ;
    else {
	o << "Coefficients: "<< endl ;
	o << f.coef ;
    }
    return o ;
}
