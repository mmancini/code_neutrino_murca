#include <cstdlib>
#include <cmath>
#include <cassert>
#include <fstream>
#include <iomanip>
#include <string>
#include "fonc.h"

using namespace std ;

// Returns value of Binomial Coefficient C(n, k)
int binomialCoeff(int n, int k)
{
  // Base Cases
  if (k==0 || k==n)
    return 1;

  // Recur
  return  binomialCoeff(n-1, k-1) + binomialCoeff(n-1, k);
}


// Function to initialize the transformation matrix Chebyshev -> Taylor expansion
//-------------------------------------------------------------------------------
void initialize_dd(Tab& dd) {
  dd = 0. ;
  int ncoef = dd.get_taille1() ;
  dd.set(0,0) = 1 ; dd.set(0, 1) = 0 ;
  dd.set(1, 0) = 0 ; dd.set(1, 1) = 1 ;
  for (int i=2; i<ncoef; i++) {
    dd.set(0,i) = 0 ;
    dd.set(1, i) = 0 ;
  }
  for (int j=2; j<ncoef; j++) {
    dd.set(j, 0) = -dd(j-2, 0) ;
    for (int i=1; i<ncoef; i++) {
      dd.set(j, i) = 2*dd(j-1, i-1) - dd(j-2, i) ; // Recursion formula
                                                   // of Chebyshev polynomials
    }
  }
}

void main_cpp(const double* alpha,const double* beta,const double* cheb, double* coeff,const int* number) {

    printf("and alpha %f\n",*alpha);
    printf("and beta %f\n",*beta);
    printf("and cheb %f\n",*cheb);
    printf("number of points %i\n",*number);
    double a = *alpha;
    double b = *beta;
    int np = *number;
    Tab valeurs(np) ;
    assert (np%2 == 1) ;
    for(int i=0;i<np;i++){valeurs.set(i) = cheb[i];}
    Fonction fi(np) ;
    Fonction x(fi.grille()) ;

#ifdef HAVE_DEVLOG
    cout << "Grille: " << endl ;
    cout << fi.grille() ;
#endif

    fi = valeurs ;
    fi.calcule_coef() ;
#ifdef HAVE_DEVLOG
    cout << fi ;
#endif
    Tab dd(np, np) ;
    initialize_dd(dd) ;

    Tab coef_Taylor(np) ;
    coef_Taylor = 0. ;
    for (int l=0; l<np; l++) {
      for (int j=0; j<np; j++)
	coef_Taylor.set(l) += fi.get_coef(j)*dd(j,l) ;
    }


    Tab coef_Taylor_enu(np) ;
    coef_Taylor_enu = 0. ;
    for (int i=0; i<np; i++) {
      for (int k=0; k<i+1; k++)
	coef_Taylor_enu.set(i-k) += pow(-b,k)/pow(a,i)*coef_Taylor(i)*binomialCoeff(i,k);
    }


#ifdef HAVE_DEVLOG
    cout << "**********************************************************" << endl ;
    ofstream fichtaylor("coefficients.d") ;
    fichtaylor.precision(30) ;
    cout << "Coefficients of the Taylor expansion: " << endl ;
    cout << setprecision(16) << coef_Taylor_enu(0) ;
    fichtaylor << -a+b << "\t" << a+b<<endl;
    fichtaylor << coef_Taylor_enu(0)<<endl ;
    //coeff[0] = coef_Taylor_enu(0);

    coeff[0] = coef_Taylor(0);
    for (int l=1; l<np; l ++) {
     cout << " + " << coef_Taylor_enu(l) << "*log(enu)^" << l ;
     fichtaylor << coef_Taylor_enu(l) << endl;
     //coeff[l] = coef_Taylor_enu(l);
     coeff[l] = coef_Taylor(l);
    }
    cout << endl ;
    cout << "Coefficients of the Taylor expansion in x: " << endl ;
    cout << setprecision(12) << coef_Taylor(0) ;
    for (int l=1; l<np; l ++) {
      cout << " + " << coef_Taylor(l) << "*X^" << l ;

    }
    cout << endl ;

    // Tab coef_Taylor_enu2(np) ;
    // coef_Taylor_enu2 = 0. ;
    // cout << "other way of Taylor coefficients"<< endl;
    // for (int i=0; i<np; i++) {
    //   for (int k=i; k<np; k++){
    // 	coef_Taylor_enu2.set(i) += pow(-b,k-i)/pow(a,k)*coef_Taylor(k)*binomialCoeff(k,i);
    //   cout <<coef_Taylor_enu2(i) << "number "<<i<<endl;}
    // }

    cout << endl ;
    cout << "**********************************************************" << endl ;

#else
    coeff[0] = coef_Taylor(0);
    for (int l=1; l<np; l ++) {
     coeff[l] = coef_Taylor(l);
    }
#endif


  return;
}

extern "C" {

  void main_c_ (double* a, double * b, double *cheb, double * coeff, int *n) {
    printf("and alpha %f\n",*a);
    printf("and beta %f\n",*b);
    printf("and number %i\n",*n);
    printf("and coeffs %f\n",cheb[*n-1]);
    printf("and coeff to be calculated %f\n",coeff[0]);


    main_cpp(a,b,cheb,coeff,n);
  }

}
