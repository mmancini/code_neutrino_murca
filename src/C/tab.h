#ifndef __TAB_H_
#define __TAB_H_

#include<cassert>
#include<iostream>
#include"arithmetic.hpp"


class Tab {

    // Donnees :
    // -------
 protected:
  int taille1 ;
  int taille2 ;
  int taille3 ;
  double* tableau ;

    // Constructeurs - Destructeur
    // ---------------------------
 public:
  explicit Tab(int dim1, int dim2=1, int dim3=1) ; //constructeur standard
  explicit Tab(const char* ) ; // constructeur depuis un fichier
  Tab(const Tab&) ; //constructeur par recopie

  virtual ~Tab() ; //destructeur

    // Affectation
    // -----------
  void operator=(const Tab&) ; //affectation depuis un autre Tab
  void operator=(double) ; //affectation depuis un double


    // Acces aux donnees
    // -----------------
  int get_taille1() const {return taille1 ; };
  int get_taille2() const {return taille2 ; };
  int get_taille3() const {return taille3 ; };
  int get_nelt() const {return taille1*taille2*taille3 ; } ;

  double operator()(int i, int j=0, int k=0) const {
    assert ((i>=0) && (i<taille1)) ;
    assert ((j>=0) && (j<taille2)) ;
    assert ((k>=0) && (k<taille3)) ;
    return tableau[(i*taille2 + j)*taille3 + k] ;
  };

  double& set(int i, int j=0, int k=0) {  //acces en ecriture...
    assert ((i>=0) && (i<taille1)) ;
    assert ((j>=0) && (j<taille2)) ;
    assert ((k>=0) && (k<taille3)) ;
    return tableau[(i*taille2 + j)*taille3 + k] ;
  };

    // Sauvegarde dans un fichier
    // --------------------------
  void sauve(const char* nom_fich) const ;

  //Multiplication membre (virtuelle)
  virtual Tab operator*(const Tab&) const ;

 protected:
  // Verif des tailles
  bool check_dims(const Tab&) const ;

  // Affichage
  virtual void affiche(std::ostream& ) const ; //pour utiliser le polymorphisme

 public:  //"public" inutile ici...
  friend std::ostream& operator<<(std::ostream&, const Tab& ) ; //la fonction externe
                                                      //a appeler
  // operations mathematiques:
  friend Tab apply(const Tab& t, double (*p_fonc)(double));
  friend Tab apply(const Tab& t1, const Tab& t2, double (*p_fonc)(double,double));
  friend Tab apply(const Tab& t, double r, double (*p_fonc)(double,double));
  friend Tab apply(double r, const Tab& t, double (*p_fonc)(double,double));
  friend double max(const Tab& t);
  friend Tab operator-(const Tab&) ;

  friend class Matrice ;
};

// arithmetic operators
double add(double r1, double r2);
double subtract(double r1, double r2);
double multiply(double r1, double r2);
double divide(double r1, double r2);

Tab operator+(const Tab&, const Tab&) ;
Tab operator+(const Tab&, double) ;
Tab operator+(double, const Tab&) ;
Tab operator-(const Tab&, const Tab&) ;
Tab operator-(const Tab&, double) ;
Tab operator-(double, const Tab&) ;
Tab operator*(const Tab&, double) ;
Tab operator*(double, const Tab&) ;
Tab operator/(const Tab&, const Tab&) ;
Tab operator/(const Tab&, double) ;
Tab operator/(double, const Tab&) ;

Tab sin(const Tab&) ;
Tab cos(const Tab&) ;
Tab tan(const Tab&) ;
Tab exp(const Tab&) ;
Tab log(const Tab&) ;
Tab sqrt(const Tab&) ;
Tab pow(const Tab&, double) ;
Tab abs(const Tab&) ;
double max(const Tab&) ;

#endif
