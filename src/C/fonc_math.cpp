#include<cmath>
#include"fonc.h"
Fonction operator-(const Fonction& f) {
    Fonction resu(-f.ff) ;
    if (f.coef_connus) {
	resu.coef = -f.coef ;
	resu.coef_connus = true ;
    }
    return resu ;
}

Fonction operator+(const Fonction& f, const Fonction& g) {
    Fonction resu(f.ff + g.ff) ;
    if ((f.coef_connus) && (g.coef_connus)) {
	resu.coef = f.coef + g.coef ;
	resu.coef_connus = true ;
    }
    return resu ;
}

Fonction operator+(const Fonction& f, double x) {
    Fonction resu(f.ff + x) ;
    if (f.coef_connus) {
	resu.coef = f.coef ;
	resu.coef.set(0) += x ;
	resu.coef_connus = true ;
    }
    return resu ;
}

Fonction operator+(double x, const Fonction& f) {
    return f+x ;
}

Fonction operator-(const Fonction& f, const Fonction& g) {
    return f + (-g) ;
}

Fonction operator-(const Fonction& f, double x) {
    return f + (-x) ;
}

Fonction operator-(double x, const Fonction& f) {
    return x + (-f) ;
}

Fonction operator*(const Fonction& f, const Fonction& g) {
    Fonction resu(f.ff * g.ff) ;
    return resu ;
}

Fonction operator*(const Fonction& f, double x) {
    Fonction resu(f.ff*x) ;
    if (f.coef_connus) {
	resu.coef = x*f.coef ;
	resu.coef_connus = true ;
    }
    return resu ;
}

Fonction operator*(double x, const Fonction&f) {
    return f*x ;
}

Fonction operator/(const Fonction& f, const Fonction& g) {
    Fonction resu(f.ff / g.ff) ;
    return resu ;
}

Fonction operator/(const Fonction& f, double x) {
    assert(x!=0.) ;
    return f*(1./x) ;
}

Fonction operator/(double x, const Fonction& f) {
    Fonction resu(x/f.ff) ;
    return resu ;
}

Fonction sin(const Fonction& f) {
    Fonction resu(sin(f.ff)) ;
    return resu ;
}

Fonction exp(const Fonction& f) {
    Fonction resu(exp(f.ff)) ;
    return resu ;
}

Fonction sqrt(const Fonction& f) {
    Fonction resu(sqrt(f.ff)) ;
    return resu ;
}

Fonction pow(const Fonction& f, double a) {
    Fonction resu(pow(f.ff, a)) ;
    return resu ;
}

Fonction abs(const Fonction& f) {
    Fonction resu(abs(f.ff)) ;
    return resu ;
}

double max(const Fonction& f) {
    return max(f.ff) ;
}

