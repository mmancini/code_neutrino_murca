#ifndef ARITHMETIC_HPP
#define ARITHMETIC_HPP

double myminus(double r);
double add(double r1, double r2);
double subtract(double r1, double r2);
double multiply(double r1, double r2);
double divide(double r1, double r2);

#endif
