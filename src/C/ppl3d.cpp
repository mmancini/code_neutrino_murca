#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>

#include "funcspec3d.hpp"

int main_cpp_3d(const int * nrx, const int * nry, const int * nrz,const double* cheb, const double* nbmin, const double* nbmax, const double* tmin, const double* tmax, const double * ymin, const double * ymax, const int* nnb, const int* knt, const int * jny, double * tabresult ) {

  int nx = *nrx + 1;
  int ny = *nry + 1;
  int nz = *nrz + 1;
  FuncSpec f(nx, ny, nz) ;

  double lognb0 = *nbmin ;
  double lognb1 = *nbmax ;
  double T0 = *tmin ;
  double T1 = *tmax ;
  double Ye0 = *ymin ;
  double Ye1 = *ymax ;

  f.set_grids(lognb0, lognb1, T0, T1, Ye0, Ye1) ;
  TabSpec lognb = f.grid_x() ;
  TabSpec temp = f.grid_y() ;
  TabSpec Ye = f.grid_z() ;

  TabSpec values(nx, ny, nz) ;
  int index;

  for (int i=0; i<nx; i++) {
    for (int j=0; j<ny; j++) {
      for (int k=0; k<nz; k++) {
	index = i + nx*(j + ny*k);
	cout << "index = " << index <<endl;
	cout << "Point at : nb = " << exp(lognb(i, j, k)) << ", T = " << exp(temp(i, j, k))
	     << ", Ye = " << Ye(i, j, k) <<" is " << cheb[index] << endl ;
	double value = cheb[index] ; 
	values.set(i, j, k) = value ;
      }
    }
  }
  f = values ;
  f.compute_coefs() ;
  int in = *nnb;
  int jt = *knt;
  int ky = *jny;

  for (int i=0; i<in; i++) {
    double nb = lognb0 + i*(lognb1-lognb0)/(in-1);
    for (int j=0; j<jt; j++) {
      double T = T0 + j*(T1-T0)/(jt-1);
      for (int k=0; k<ky; k++) {
	double ye = Ye0 + k*(Ye1-Ye0)/(ky-1);
	cout << "Point at : nb = " << exp(nb) << ", T = " << exp(T)
	     << ", Ye = " << ye << endl ;
	double value = f.compute_in_xyz(nb, T, ye);
	index = i + in*(j + jt*k);
	tabresult[index] = value;
	cout << "The value for index "<< index << " is : " << value << endl ;
      }
    }
  }
  cout << "Tabresult entry 0 " <<tabresult[0] << endl;
  return EXIT_SUCCESS ; 
}

extern "C" {

  void main_c_3d_(const int * nrx, const int * nry, const int * nrz,const double* cheb, const double* nbmin, const double* nbmax, const double* tmin, const double* tmax, const double * ymin, const double * ymax, const int* nnb, const int* knt, const int * jny, double * tabresult ) {
    printf("first value %f\n",cheb[0]);
    printf("and number of points in nb %i\n",*nrx);
    printf("and number of points in T %i\n",*nry);
    printf("and number of points in Ye %i\n",*nrz);

    main_cpp_3d(nrx, nry,  nrz, cheb,  nbmin,  nbmax,  tmin,  tmax, ymin, ymax, nnb, knt,  jny, tabresult) ;
  }

}
