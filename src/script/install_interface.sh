#!/bin/bash

# Crea l'ambiente virtuale
python -m venv venv

# Attiva l'ambiente virtuale
source venv/bin/activate

# Aggiorna pip e installa le dipendenze
pip install --upgrade pip
pip install -r requirements.txt  # crea un file requirements.txt con le tue dipendenze

# # Installa pyinstaller
# pip install pyinstaller

# # Crea gli eseguibili con PyInstaller
# pyinstaller --onefile app.py
# pyinstaller --onefile neutrino_launcher.py

# Disattiva l'ambiente virtuale
# deactivate

#echo "Installazione completata. Gli eseguibili si trovano nella directory 'dist'."
