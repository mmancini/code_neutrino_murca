#!/bin/bash
length=$(($#))
array=${@:1:$length}
#proc=$((${@:$length+1}-1))
echo ${@:2:1}
proc=$(grep -ih 'CHEB proc = ' ${@:1:1} |awk 'BEGIN{a=   0}{if ($4>0+a) a=$4} END{print a+1}')
echo "Detected procs: $proc"

printf " %3s  %10s %5s \n" 'proc' 'sum/nn(s)' 'nn'

for ii in $array; do
  [[ ! -f "$ii" ]] && echo "file $ii does not exist" && exit
done


#echo $array $proc
filetmp='/tmp/measure_time_bak'
grep -ih time $array > ${filetmp}

npttot=$(wc -l ${filetmp}|awk '{print $1}')
for ii in $(seq 0 $proc);do
      awk '{if($3==proc) {sum+=$8; nn+=1}} END {if(nn>0) printf " %3d   %10.3f %5d \n", proc,  sum/nn, nn; }' proc="$ii" ${filetmp}
done
echo " Total npt= $npttot"
