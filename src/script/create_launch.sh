#!/bin/bash

# create launch script and all needed for neutrino simulation on
# cluster at FIXED TEMPERATURE

# create dir
TEMP=$1
dir=$(printf "t%03d" $TEMP)
EINNR=einnr_$dir
NUFILEDIR=nu_files

[[ -d $dir ]] && echo "directory $1 exists yet : exit " && exit
mkdir -p $dir


# add link for eos* files
cd $dir
for ii in ../eos.* ; do
    ln -s $ii .
done
ln -s ../para*.dat .

# create input file
sed  "s/TEMP/$TEMP/g" ../einnr_tFIX > $EINNR

# create nu_files directory
mkdir $NUFILEDIR


cat << EOF > launch_hybrid.slurm
#!/bin/bash
#SBATCH --partition=def
#SBATCH --qos=mesopsl2_def_long
#SBATCH --clusters=mesopsl2
#SBATCH --nodes=6
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=8
#SBATCH -J cdi-$TEMP
#SBATCH --account=oertel
#SBATCH --time=24:00:00

module load intel
module load mkl
module load openmpi
module load hdf5


# exec
export EXEC=/obs/oertel/Neutrino/code_neutrino/BUILD/neutrino

# Sans hyperthreading
export OMP_NUM_THREADS=\$SLURM_CPUS_PER_TASK
/usr/bin/time -o codinotime.log  mpirun -n \$SLURM_NTASKS \$EXEC  --dir $NUFILEDIR < $EINNR

EOF
