# Makefile for compiling the code for neutrino rates
# ==============================
#
# Options:
# --------
#
# BOUNDCHECK=(0|1)
#
# Note: Switch on bound checking for Fortran part.
#
# HDF5=(0|1)
#
# Enable writing in HDF5-format for the data tables
BOUNDCHECK = 1
HDF5 = 0


program := neutrino
OBJDIR := BUILD
SRCDIR := src




VPATH = $(SRCDIR)/C : $(SRCDIR)/F90
src_c    := ppl.cpp fonc.cpp fonc_math.cpp tab.cpp tab_math.cpp matrice.cpp mat_lapack.cpp arithmetic.cpp ppl3d.cpp funcder3d.cpp funccoefs3d.cpp funcmath3d.cpp funcspec3d.cpp funcspec_interp3d.cpp tabmath3d.cpp tabspec3d.cpp
src_bad  := tabspec.cpp tabmath_badpoints.cpp arithmetic.cpp
src_f1    := types_modules.F90 \
	    nu_omp.F90 \
	    nu_mpi_info.F90 \
	    nu_parallel.F90 \
	    vectors.F90 \
            c2cray.F90 \
	    import_c.F90 \
	    common_variables.F90 \
	    OrderingBorders.F90 \
	    m_dichotomy.F90 \
            leptons.F90 \
	    virial_eos.F90 \
	    integration_densities.F90 \
	    skyrmeparameter.F90 \
	    lindhard_function.F90 \
	    m_eos.F90 \
            apr_eos.F90 \
            tools_files.F90 \
            integrals.F90 \
            inttest.F90 \
	    matrix_elements.F90 \
	    murca_debug.F90 \
	    murca_hadrons.F90  \
	    grid_construction.F90 \
	    murca_leptons_GL_integration.F90  \
	    murca_leptons_fermi_surface.F90  \
	    murca_leptons.F90  \
            calculate.F90 \
	    main_durca.F90 \
	    main_murca.F90 \
	    fermiSpeed.F90

ifeq ($(HDF5),1)
	src_f = modhdf5.F90  $(src_f1)
else
	src_f = $(src_f1)
endif


version_git := $(OBJDIR)/gitversion.h

src_neutrino = neutrino_main.F90
src_merger   = merger_main.F90
src_merger2   = merger2.F90
src_haupt = analyse.F90
src_badpoints = badpoints.cpp

obj_f  := $(addprefix $(OBJDIR)/,$(subst .F90,.o,$(src_f)))
obj_c  := $(addprefix $(OBJDIR)/,$(subst .cpp,.o,$(src_c)))
obj_bad  := $(addprefix $(OBJDIR)/,$(subst .cpp,.o,$(src_bad)))

obj_f_neutrino := $(obj_f) $(addprefix $(OBJDIR)/,$(subst .F90,.o,$(src_neutrino)))
obj_f_merger := $(obj_f) $(addprefix $(OBJDIR)/,$(subst .F90,.o,$(src_merger)))
obj_f_merger2 := $(obj_f) $(addprefix $(OBJDIR)/,$(subst .F90,.o,$(src_merger2)))
obj_f_haupt := $(obj_f) $(addprefix $(OBJDIR)/,$(subst .F90,.o,$(src_haupt)))
obj_badpoints := $(obj_bad) $(addprefix $(OBJDIR)/,$(subst .cpp,.o,$(src_badpoints)))

#include intel.mk
include config/gnu.mk

MAKEMOD.f08 = $(FC) $(FCFLAGS) $(TARGET_ARCH) -fsyntax-only -c



ifeq ($(HDF5),1)
   FC = h5pfc -DHAVE_HDF5
   HDF5_LIB = -lhdf5
   HDF5_C = -Dhdf5
else
   FC = mpif90
   HDF5_LIB =
   HDF5_C =
endif

INCLUDE =

$(program):  make_obj_dir $(version_git) $(obj_f_neutrino) $(obj_c)
	@echo "--------------------------------"
	@echo Linking : $a
	$(LINK)  $(INCLUDE)  -o $(OBJDIR)/$(program) $(obj_f_neutrino) $(obj_c) $(LD_LIB)

neutrino_debug: $(version_git) $(obj_f_neutrino) $(obj_c)
	@echo "--------------------------------"
	@echo Linking : $a
	$(LINK) -o $(OBJDIR)/neutrino_debug $(obj_f_neutrino) $(obj_c) $(LD_LIB)

neutrino_chi: $(version_git) $(obj_f_neutrino) $(obj_c)
	@echo "--------------------------------"
	@echo Linking : $a
	$(LINK) -o $(OBJDIR)/neutrino_chi $(obj_f_neutrino) $(obj_c) $(LD_LIB)


merger: $(version_git) $(obj_f_merger) $(obj_c)
	@echo "--------------------------------"
	@echo Linking : $@
	$(LINK) -o $(OBJDIR)/merger $(obj_f_merger) $(obj_c) $(LD_LIB)

merger2: $(version_git) $(obj_f_merger2) $(obj_c)
	@echo "--------------------------------"
	@echo Linking : $@
	$(LINK) -o $(OBJDIR)/merger2 $(obj_f_merger2) $(obj_c) $(LD_LIB)


haupt: $(version_git) $(obj_f_haupt) $(obj_c)
	@echo "--------------------------------"
	@echo Linking : $@
	$(LINK) -o $(OBJDIR)/haupt $(obj_f_haupt) $(obj_c) $(LD_LIB)

badpoints: $(version_git) $(obj_badpoints)
	@echo "--------------------------------"
	@echo $(obj_bad)
	@echo Linking : $@
	$(LINK) -o $(OBJDIR)/badpoints $(obj_badpoints) $(LD_LIB)

$(version_git): .git/HEAD .git/index
	@echo "#ifndef GIT_VERSION " > $@
	@echo "# define GIT_VERSION 1\n"\
	      " !const char *gitversion = \"$(shell git rev-parse HEAD)\" \n"\
	      " character(*),parameter :: gitversion = \"$(shell git rev-parse HEAD)\";" >> $@
	@echo "#endif" >> $@

make_obj_dir :
	@mkdir -p $(OBJDIR)

doc :
	@DOXYGEN=$$(which doxygen);\
	if [ -z $${DOXYGEN} ]; then \
	    echo "Doxygen is not installed";\
	    echo "$(DOXYGEN)" ;\
	 else \
	    echo "Doxygen is installed"; \
	    echo "$(DOXYGEN)" ;\
	    cp $(SRCDIR)/Doxyfile $(OBJDIR)/. ;\
	    cd $(OBJDIR) ;\
	    sed -i "" "s:NEUTRINO_SRC_PATH:../$(SRCDIR):" Doxyfile ; \
	    doxygen Doxyfile ;\
	 fi


all: $(program) merger merger2 haupt badpoints


.PHONY: $(OBJDIR) doc



$(OBJDIR)/%.o : %.cpp
	$(CC) $(CC_FLAGS) $< -o $@

$(OBJDIR)/%.o : %.F90
	$(FC) $(FC_FLAGS) $(INCLUDE) -o $@ $<
	@touch $@

$(OBJDIR)/%.smod $(OBJDIR)/%.mod: %.F90
	$(MAKEMOD.f08) $<


%.anc: %.F90
	$(MAKEMOD.f08) $<
	@touch $@

clean:
	@echo cleaning $(OBJDIR)
	@cd $(OBJDIR); \
	rm -f *.o *.mod $(program) merger merger2
