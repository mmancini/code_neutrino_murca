Title: Neutrino-nucleon interactions in dense and hot matter

Abstract: Neutrinos play an important role in compact star
astrophysics: neutrino-heating is one of the main ingredients in
core-collapse supernovae, neutrino-matter interactions determine the
composition of matter in binary neutron star mergers and have among
others a strong impact on conditions for heavy element nucleosynthesis
and neutron star cooling is dominated by neutrino emission except for
very old stars. Many works in the last decades have shown that in
dense matter medium effects considerably change the neutrino-matter
interaction rates, whereas many astrophysical simulations use analytic
approximations which are often far from reproducing more complete
calculations. The aim of the present project is to perform the latter
calculations for the different relevant thermodynamic conditions and
provide the corresponding data together with the equation(s) of state
on the Compose data base (https://compose.obspm.fr).
