## A future alternative to Makefile for <span style="color:Blue">Neutrino code</span>

To install "neutrino code," in addition to the Makefile, we are
experimenting with the Fortran Package Manager
[FPM](https://fpm.fortran-lang.org/index.html), which allows for
easier installation of the code and all its dependencies in a more
modern way.

First you should have installed the "fpm" package using "pip":
```bash
python -m venv venv
source venv/bin/activate
pip install fpm
```

To compile and install :
```bash
source config/config_fpm.sh
```




## Compilation with Macros
To have other precompilation macro activated, you should add them to the list "macros" in "fpm.toml".
The list of possible macros is given here:
- DURCA_DEBUG
- MC_DEBUG
- DEBUG
- VECTOR
- HAVE_DEBUG_VERBOSE_1TH

## Clean the build
To clean up the build:
```fpm clean --all```
