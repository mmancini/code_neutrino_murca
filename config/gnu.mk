ifeq ($(BOUNDCHECK),1)
   FC_FLAGS_BOUNDCHECK = -pg -g -O0 --debug -DMC_DEBUG -DDEBUG -Wall -fbounds-check -fbacktrace   -Wuninitialized -pedantic -DHAVE_DEBUG_VERBOSE_1TH -DVARIABLE_WIDTH
else
   FC_FLAGS_BOUNDCHECK =  #-g -pg  -O2
endif
# Flags for Murca integration with different options
FC_FLAGS_MURCA =   -DVECTOR
# gnu
FC_FLAGS = -c -g   -fopenmp -std=f2008 $(FC_FLAGS_BOUNDCHECK) $(FC_FLAGS_MURCA) $(HDF5_LIB) -J$(OBJDIR)
CC = g++
CC_FLAGS = -c -g -cpp
LINK = $(FC)
LD_LIB = -fopenmp -lstdc++ -llapack -lblas -lgfortran $(FC_FLAGS_BOUNDCHECK) -lm -lfftw3  $(HDF5_LIB) -I$(OBJDIR)
