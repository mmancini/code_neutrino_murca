ifeq ($(BOUNDCHECK),1)
   FC_FLAGS_BOUNDCHECK = -DDEBUG -check bounds -traceback -O0  #-DHAVE_DEVLOG
else
   FC_FLAGS_BOUNDCHECK =   #-O2
endif
# Flags for Murca integration with different options
FC_FLAGS_MURCA =  -DVECTOR


# intel
FC_FLAGS = -c -g -fopenmp -std08 $(FC_FLAGS_BOUNDCHECK)  $(FC_FLAGS_MURCA) $(HDF5_LIB) -module $(OBJDIR)
CC = icc
CC_FLAGS = -c -g -ipp -Wall
LINK = $(FC)
LD_LIB = -fopenmp -lstdc++ -mkl -lgfortran $(FC_FLAGS_BOUNDCHECK) -lm $(HDF5_LIB) -I$(OBJDIR)
