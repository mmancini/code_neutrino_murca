#!/usr/bin/env bash

echo "Searching FPM ...."

if ! command -v fpm > /dev/null 2>&1; then
	echo "FPM is not found"
	echo "Searching for VIRTUALENV"	
	echo $VIRTUAL_ENV
	if [ -n "$VIRTUAL_ENV" ]; then
		echo "Virtualenv is sourced but does not contain FPM"
		echo "Trying to install it"
#		pip install fpm
	else
		echo "VIRTUALENV NOT SOURCED"
		echo "Please source it or create and source"
		return
	fi

fi


echo "Exporting variables for FPM compilation : FPM_FFLAGS="$(python3 ./config/config.py)
echo
export FPM_FFLAGS=$(python3 ./config/config.py)
echo "Now you can run: \"fpm install\""
