import os

stream = os.popen('h5pfc --show')
h5pfc_show = stream.read()
h5pfc_array = h5pfc_show.split(" ")

flag = ""
incpath = ""
libpath = ""
liblist = ""
for word in h5pfc_array:
  if word.startswith('-I'):
    incpath += word
    incpath += " "
  if word.startswith('-L'):
    libpath += word
    libpath += " " 
  if word.endswith('.a'):
    lib = word.split('/')
    libname = lib[-1].replace('.a','')
    if libname.startswith('lib'):
      liblist += libname.replace('lib','-l') + " "
    libdir = '/'.join(lib[:-1])
    libdir = '-L'+libdir
    if libdir not in libpath:
      libpath += libdir
      libpath += " "
    
print(incpath + libpath + liblist)
