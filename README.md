# <span style="color:Blue">Neutrino-nucleon interactions in dense and hot matter</span>

## <span style="color:Blue">Abstract</span>
Neutrinos play an important role in compact star
astrophysics: neutrino-heating is one of the main ingredients in
core-collapse supernovae, neutrino-matter interactions determine the
composition of matter in binary neutron star mergers and have among
others a strong impact on conditions for heavy element nucleosynthesis
and neutron star cooling is dominated by neutrino emission except for
very old stars. Many works in the last decades have shown that in
dense matter medium effects considerably change the neutrino-matter
interaction rates, whereas many astrophysical simulations use analytic
approximations which are often far from reproducing more complete
calculations. The aim of the present project is to perform the latter
calculations for the different relevant thermodynamic conditions and
provide the corresponding data together with the equation(s) of state
on the [CompOSE](https://compose.obspm.fr) data base.

## Documents

- [Code](##code)
- [List of compilation macros](#List-of-compilation-macros)
- [Fortran Package Manager](./doc/FortranPackageManager.md)

## <span style="color:Blue">Code</span><a name=code></a>
Cette version est adapté pour tourner avec l'EoS 123 de Compose, sinon
il faut d'autres fichiers de paramétres.

Par contre, je travaille encore sur la partie RPA: pour l'instant le
calcul d'un point en T, n_B , Y_e prend environ une heure et il faut
calculer cela pour tous les points de la table. Si tu veux, on peut en
discuter la semaine prochaine.

Ce qui marche déjà, c'est la partie "non relativistic" et
"relativistic", valeur 1 ou 3 respectivement de la variable "choice"
dans le fichier para_num.dat. Pour l'instant je le lance à l'aide du
fichier "einnr" pour donner toutes les entrées nécessaires. C'est
certainement pas idéal comme beaucoup d'autres choses dans le code. Il
calcule un point en T, n_B, Y_e actuellement. Les boucles de la routine
"write_chebichev" sont limités par les indices dans les trois dernières
lignes dans "einnr"

```bash
indexT_min,indexT_max

indexnb_min,indexnb_max

indexye_min,indexye_max
```

Chaque point est indépendent, donc la plus simple paralelisation ne
concerne que les boucles dans "write_chebichev"

J'avoue que je n'ai pas écrit ce code pour le faire lire par quelqu'un
d'autre, mais comme je me rends compte que les calculs prennent beaucoup
de temps, je suis preneuse de tout conseil. Il ya certainement beaucoup
de choses à améliorer.

Pour faciliter la lecture du code, j'ai attaché aussi quelques notes sur
les calculs à la base.


#### To merge additional files:

1. mettre tous les fichiers dans nu_files
2. module load python/3.6.4 !! nécessaire que sur mesopsl
3. ../src/python/neutrinoTool.py --dir nu_files/ --mode=all

**Obsolete:** To launch with einnr input :
```bash
mpirun --bind-to none -n 1 ../BUILD/neutrino < einnr
```

#### On mesopsl:

```bash
module load intel/21.1.1  openmpi/4.1.0
module load hdf5
module load mkl
```

To create a simulation for a fixed TEMP, in new directory MYSIMU:
1.create a file einnr_tFIX

```bash
cat einnr_tFIX
2
T
60,100,5
1
1
0.18651673489927043
-58.51
TEMP,TEMP
100,140
1,60
```

2.Put the **eos.* ** and **para*.dat** files in **MYSIMU**

```bash
ls MYSIMU
einnr_tFIX  eos.nb  eos.t  eos.thermo  eos.yq  para_num.dat  para_skyrme.dat
```

Two example files for para_skyrme exist (sly4 and sly5) with two version for handling the spin channel with option MIGDAL

3.Launch the creation script

```bash
cd MYSIMU
~/Codino_new/src/script/create_launch.sh 55
ls .
einnr_tFIX  eos.nb  eos.t  eos.thermo  eos.yq  para_num.dat  para_skyrme.dat  t055
cd t055
ls
einnr_t055  eos.t       eos.yq               nu_files      para_skyrme.dat
eos.nb      eos.thermo  launch_hybrid.slurm  para_num.dat
```

4. modify launch script to adjust the number of nodes or for the restart:
launch_hybrid.slurm
change the line ```#SBATCH --nodes=6``` or for the restart, the line:

```bash
usr/bin/time -o codinotime.log  mpirun -n $SLURM_NTASKS $EXEC  --dir nu_files < einnr_t055
```

to

```bash
/usr/bin/time -o codinotime.log  mpirun -n $SLURM_NTASKS $EXEC --restart --dir nu_files < einnr_t055
```

#### To use the account of the luth, change line:

```bash
#SBATCH --partition=hi
#SBATCH --qos=mesopsl2_luth
#SBATCH --clusters=mesopsl2
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=6
#SBATCH --account=luth
#SBATCH -J cdi-1
#SBATCH --time=24:00:00
```

- ```squota``` for hours available
- To launch : ```sbatch launch_hybrrid.slurm```.
- ```scancel``` to cancel a waiting job

## List of compilation macros

default off:

**DURCA_DEBUG** -> enable a huge number of additional writing for debugging the Durca code
**MC_DEBUG** -> fix the seed for MC generation of integration variables in Murca for debugging
**NESAMPLING** -> perform the leptonic integration in Murca with sampling over ne (not 1/cosh)

default on:
**VECTOR** -> use the dependence on momentum exchange between nucleons in Murca
**INT_CM** -> use the center of mass prescription for generating momenta in Murca

## Installing web interface

Launch the script "src/script/install_interface.sh":

```bash
./script/install_interface.sh
```

## Conda environement

To run the code, one can create a conda environment in which all the necessary packages are installed.
To create the environment on your machine:
        - make sure you have that conda is installed on your machine,
        - enter the command `conda env create --name murca-env --file=dependencies.yml`
To activate the environment, type `conda activate murca-env`.
To deactivate the environemnt, type `conda deactivate`.
To remove the environment and all its content, type `conda remove --name murca-env --all`.

Once you have activated the environement, you need to make two changes in the Makefile:
        - remove the hdf5 option in the Makefile: at the beginning of the Makefile, there is an option to put it to zero,
        - there may be an incompatibility with PIE which will prevent you from compiling the code. In `config/gnu.mk`, put in FC_FLAGS and CC_FLAGS the additional flag -fPIE or -no-pie.

Then you can compile and run the code.

### Condor submission for LIGO clusters

The CIT and LLO (and any other LIGO related clusters) use CONDOR (not SLURM) for job submission.
Therefore you have to create a condor submission file we will call `condor.submit` and launch it with the command `condor_submit condor.submit`. To check your job in the queue type `condor_q` and to remove it type `condor_rm .....` with your job number.
This file should look something like this:

```bash
universe                 = vanilla
get_env                  = True
executable               = /home/.........../code_neutrino_murca/........./script.sh
log                      = /home/.........../code_neutrino_murca/........./logs/try_murca.log
output                   = /home/.........../code_neutrino_murca/........./logs/try_murca.out
error                    = /home/.........../code_neutrino_murca/........./logs/try_murca.err
stream_error = True
stream_input = True
stream_output = True
request_cpus = 16
request_disk = 10 MB
request_memory = 2048 MB
should_transfer_files = yes
when_to_transfer_output = ON_EXIT
accounting_group        = ligo.sim.o4.cbc.extremematter.bilby
batch_name = try_durca_code
queue 1
```

You need to change the paths of your executable and output files accordingly.
Log and error file will be create to follow how the submission and the code is going.
Do not forget to create a "logs" directory or you will get an error upon submission.
You can change the batch name and the output log file names.
In this case, the file `script.sh` contains the mpirun command to launch the code; carefull you may need to chmod+x for permission.
The script should include the header `#!/bin/sh` without which condor will raise an issue and will not recognize the script as an executable. It should look something like this:

```bash
#!/bin/sh
PATH_EXE=/home/........./code_neutrino_murca/BUILD/neutrino
PATH_IN=/home/........../code_neutrino_murca/................./input.nl
mpirun --oversubscribe -n 1 ${PATH_EXE} -i ${PATH_IN}
```

You can change the `-n` as you wish, and you must fill the `PATH_EXE` and `PATH_IN` according to the location of your executable and input files respectively.

### Slurm submission

To send a job with slurm within the conda environment, you will need to write a script in shell named `whatever.sh` and contains the following:

```bash
#!/bin/bash
#SBATCH -J try_murca                  # Job name
#SBATCH -o try_murca.out              # Output file name
#SBATCH -e try_murca.err              # Error file name
#SBATCH -p ........                   # partition to use on the machine
#SBATCH --nodes=....                  # number of nodes
#SBATCH --ntasks-per-node=....
#SBATCH --cpus-per-task=....
#SBATCH -t 128:0:00                   # Allotted run time (1 week=128)

umask 0022                            # read permission for others
module purge
conda activate murca-env              # activate the conda environment
./script.sh                           # this is the script that executes the code
```

To run a job, use the command `sbatch whatever.sh` and to check it in the queue type `squeue`.

## Download the EoS

The code requires equation of state input in the CompOSE format. First create a folder with the EoS name, this is where you will launch the code.
Then you have to download the EoS data. For that, you can use the command wget followed by the address of the EoS files eos.compo, eos.thermo, eos.micro, eos.nb, eos.t and eos.yq, for e.g. for the EoS SFHo with electrons type:
- ```wget https://compose.obspm.fr/download/3D/Hempel_SchaffnerBielich/sfhoy/with_electrons/eos.compo```
- ```wget https://compose.obspm.fr/download/3D/Hempel_SchaffnerBielich/sfhoy/with_electrons/eos.micro``` etc.
Downloading of eos.thermo, eos.compo and eos.micro can take a while.
